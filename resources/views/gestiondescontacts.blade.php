@extends('layouts.app')

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"/>

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                @if(Auth::user()->type_compte == 'entreprise')
                    <buttom class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"> Ajouté un contact</i>
                    </buttom>
                @else
                    <buttom class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2"><i class="fa fa-user"> Ajouté un contact</i>
                    </buttom>
                @endif
                </div>
                <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Liste des Contacts</h4>
            </div>
            <!--  AJOUTER UN NEW CONTACT DEBUT ENTREPRISE-->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Nouveau contact</h4>

                        </div>
                        <div class="modal-body">
                            <form action="{{ route('validdescontacts') }}" method="post" autocomplete="off">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="nom" name="nom_contact" placeholder="Nom *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="prenoms" name="prenoms_contact" placeholder="Prenoms *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-sm" id="email" name="email_contact" placeholder="Email *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="poste" name="fonction_contact" placeholder="Fonction">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" name="numero_contact" placeholder="Contact">
                                </div>
                                <!-- <div class="form-group">
                                    <label for="numero_contact">Contact</label>
                                    <input type="text" class="form-control input-sm phone" id="phone-mask" name="numero_contact" required="required" data-format="(+999) 99-99-99-99" data-placeholder="Contact">
                                    <p class="help-block">Entrer le contact comme : 00000000</p>
                                </div> -->
                                <div class="form-group">
                                    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-sm btn-primary" name="save"><i class="fa fa-user"></i> Créer</button>
                                    <button data-dismiss="modal" class="btn btn-sm btn-default" href="#">Quitter</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!--  AJOUTER UN NEW CONTACT FIN ENTREPRISE-->

            <!--  AJOUTER UN NEW CONTACT DEBUT-->
            {{--<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Nouveau contact</h4>

                        </div>
                        <div class="modal-body">
                            <form action="{{ route('validdescontactsparticulier') }}" method="post" autocomplete="off">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="nom" name="nom_contact" placeholder="Nom *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="prenoms" name="prenoms_contact" placeholder="Prenoms *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-sm" id="email" name="email_contact" placeholder="Email *" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="poste" name="fonction_contact" placeholder="Fonction">
                                </div>
                                <div class="row">                                    
                                    <div class="form-group">                                    
                                        <div class="col-md-4 col-sm-4">
                                            <label>Indicatif</label>
                                            <select class="form-control" name="indic_contact">
                                              <option value="225">-Pays-</option>
                                              <!-- <optgroup label="A">
                                                <option value="93">Afghanistan </option>
                                                <option value="27">Afrique_du_Sud </option>
                                                <option value="355">Albanie </option>
                                                <option value="213">Algerie </option>
                                                <option value="49">Allemagne </option>
                                                <option value="376">Andorre </option>
                                                <option value="244">Angola </option>
                                                <option value="1264">Anguilla </option>
                                                <option value="966">Arabie_Saoudite </option>
                                                <option value="54">Argentine </option>
                                                <option value="374">Armenie </option>
                                                <option value="297">Aruba </option>
                                                <option value="247">Ascension </option>
                                                <option value="61">Australie </option>
                                                <option value="43">Autriche </option>
                                                <option value="994">Azerbaidjan </option>
                                              </optgroup> -->
                                              <!-- <optgroup label="B">
                                                <option value="1242">Bahamas </option>
                                                <option value="880">Bangladesh </option>
                                                <option value="1246">Barbade </option>
                                                <option value="973">Bahrein </option>
                                                <option value="32">Belgique </option>
                                                <option value="501">Belize </option> -->
                                                <option value="229">Benin </option>
                                               <!--  <option value="1441">Bermudes </option>
                                                <option value="375">Bielorussie </option>
                                                <option value="95">Birmanie </option>
                                                <option value="591">Bolivie </option>
                                                <option value="267">Botswana </option>
                                                <option value="975">Bhoutan </option>
                                                <option value="387">Boznie_Herzegovine </option>
                                                <option value="55">Bresil </option>
                                                <option value="673">Brunei </option>
                                                <option value="359">Bulgarie </option>
                                                <option value="226">Burkina_Faso </option>
                                                <option value="257">Burundi </option>
                                              </optgroup> -->
                                              <!-- <optgroup label="C">
                                                <option value="855">Cambodge </option>
                                                <option value="237">Cameroun </option>
                                                <option value="1">Canada </option>
                                                <option value="1345">Canaries </option>
                                                <option value="238">Cap_Vert </option>
                                                <option value="56">Chili </option>
                                                <option value="86">Chine </option>
                                                <option value="357">Chypre </option>
                                                <option value="57">Colombie </option>
                                                <option value="269">Comores </option>
                                                <option value="242">Congo </option>
                                                <option value="243">Congo_democratique </option>
                                                <option value="682">Cook </option>
                                                <option value="850">Coree_du_Nord </option>
                                                <option value="82">Coree_du_Sud </option>
                                                <option value="506">Costa_Rica </option> -->
                                                <option value="225">Côte_d_Ivoire </option>
                                                <!-- <option value="385">Croatie </option> -->
                                                <!-- <option value="53">Cuba </option> -->
                                                <!-- <option value="599">Curaçao </option> -->
                                              <!-- </optgroup> -->
                                              <!-- <optgroup label="D"> -->
                                                <!-- <option value="45">Danemark </option> -->
                                                <!-- <option value="253">Djibouti </option> -->
                                                <!-- <option value="1767">Dominique </option> -->
                                              <!-- </optgroup> -->
                                              <!-- <optgroup label="E"> -->
                                                <!-- <option value="20">Egypte </option> -->
                                                <!-- <option value="971">Emirats_Arabes_Unis </option> -->
                                                <!-- <option value="593">Equateur </option> -->
                                                <!-- <option value="291">Erythrée </option> -->
                                                <!-- <option value="34">Espagne </option> -->
                                                <!-- <option value="372">Estonie </option> -->
                                                <!-- <option value="1">Etats_Unis </option> -->
                                                <!-- <option value="251">Ethiopie </option> -->
                                              <!-- </optgroup> -->
                                              <!-- <optgroup label="F"> -->
                                                <!-- <option value="298">Feroe </option> -->
                                                <!-- <option value="679">Fidji </option> -->
                                                <!-- <option value="358">Finlande </option> -->
                                                <!-- <option value="33">France </option> -->
                                              <!-- </optgroup> -->
                                              <!-- <optgroup label="G">                                                 -->
                                                    <!-- <option value="241">Gabon </option> -->
                                                    <!-- <option value="220">Gambie </option>
                                                    <option value="995">Géorgie </option>
                                                    <option value="233">Ghana </option>
                                                    <option value="350">Gibraltar </option>
                                                    <option value="30">Grece </option>
                                                    <option value="1473">Grenade </option>
                                                    <option value="299">Groenland </option>
                                                    <option value="590">Guadeloupe </option>
                                                    <option value="1671">Guam </option>
                                                    <option value="502">Guatemala</option>
                                                    <option value="224">Guinee </option>
                                                    <option value="245">Guinee_Bissau </option>
                                                    <option value="240">Guinee_Equatoriale </option>
                                                    <option value="592">Guyana </option>
                                                    <option value="594 ">Guyane_Francaise </option>
                                                </optgroup>
                                                <optgroup label="H">
                                                    <option value="509">Haiti </option>
                                                    <option value="504">Honduras </option>
                                                    <option value="852">Hong_Kong </option>
                                                    <option value="36">Hongrie </option>
                                                </optgroup>
                                                <optgroup label="I">                                                    
                                                    <option value="91">Inde </option>
                                                    <option value="62">Indonesie </option>
                                                    <option value="964">Iran </option>
                                                    <option value="98">Iraq </option>
                                                    <option value="353">Irlande </option>
                                                    <option value="354">Islande </option>
                                                    <option value="972">Israel </option>
                                                    <option value="39">italie </option>
                                                </optgroup>
                                                <optgroup label="J"> -->
                                                    <!-- <option value="1876">Jamaique </option>
                                                    <option value="81">Japon </option>
                                                    <option value="962">Jordanie </option>
                                                </optgroup>
                                                <optgroup label="K">
                                                    <option value="7">Kazakhstan </option>
                                                    <option value="254">Kenya </option>
                                                    <option value="996">Kirghizistan </option>
                                                    <option value="686">Kiribati </option>
                                                    <option value="383">Kosovo </option>
                                                    <option value="965">Koweit </option>
                                                </optgroup>
                                                <optgroup label="L">
                                                    <option value="856">Laos </option>
                                                    <option value="266">Lesotho </option>
                                                    <option value="371">Lettonie </option>
                                                    <option value="961">Liban </option>
                                                    <option value="231">Liberia </option>
                                                    <option value="218">Libye </option>
                                                    <option value="423">Liechtenstein </option>
                                                    <option value="370">Lituanie </option>
                                                    <option value="352">Luxembourg </option>                                             
                                                </optgroup>       
                                                <optgroup label="M">                                         
                                                    <option value="853">Macao </option>
                                                    <option value="389">Macedoine </option>
                                                    <option value="261">Madagascar </option>
                                                    <option value="60">Malaisie </option>
                                                    <option value="265">Malawi </option>
                                                    <option value="960">Maldives </option>
                                                    <option value="223">Mali </option>
                                                    <option value="500">Malouines </option>
                                                    <option value="356">Malte </option>
                                                    <option value="1670">Mariannes du Nord </option>
                                                    <option value="212">Maroc </option>
                                                    <option value="692">Marshall </option>
                                                    <option value="596">Martinique </option>
                                                    <option value="230">Maurice </option>
                                                    <option value="222">Mauritanie </option>
                                                    <option value="262">Mayotte </option>
                                                    <option value="52">Mexique </option>
                                                    <option value="691">Micronésie </option>
                                                    <option value="373">Moldavie </option>
                                                    <option value="377">Monaco </option>
                                                    <option value="976">Mongolie </option>
                                                    <option value="382">Monténégro </option>
                                                    <option value="1664">Montserrat </option>
                                                    <option value="258">Mozambique </option>
                                                </optgroup>
                                                <optgroup label="N">
                                                    <option value="264">Namibie </option>
                                                    <option value="674">Nauru </option>
                                                    <option value="977">Nepal </option>
                                                    <option value="505">Nicaragua </option>
                                                    <option value="227">Niger </option>
                                                    <option value="234">Nigeria </option>
                                                    <option value="683">Niue </option>
                                                    <option value="47">Norvege </option>
                                                    <option value="687">Nouvelle_Caledonie </option>
                                                    <option value="64">Nouvelle_Zelande </option>
                                                </optgroup>
                                                <optgroup label="O">
                                                    <option value="968">Oman </option>
                                                    <option value="256">Ouganda </option>
                                                    <option value="998">Ouzbekistan </option>
                                                </optgroup>
                                                <optgroup label="P">
                                                    <option value="92">Pakistan </option>
                                                    <option value="680">Palaos </option>
                                                    <option value="970">Palestine </option>
                                                    <option value="507">Panama </option>
                                                    <option value="675">Papouasie_Nouvelle_Guinee </option>
                                                    <option value="595">Paraguay </option>
                                                    <option value="31">Pays_Bas </option>
                                                    <option value="51">Perou </option>
                                                    <option value="63">Philippines </option>                                              
                                                    <option value="64">Pitcairn </option>
                                                    <option value="48">Pologne </option>
                                                    <option value="689">Polynesie </option>
                                                    <option value="1787">Porto_Rico </option>
                                                    <option value="351">Portugal </option>
                                                </optgroup>
                                                <optgroup label="Q">
                                                    <option value="974">Qatar </option>
                                                </optgroup>
                                                <optgroup label="R">
                                                    <option value="1809">Republique_Dominicaine </option>
                                                    <option value="420">Republique_Tcheque </option>
                                                    <option value="262">Reunion </option>
                                                    <option value="40">Roumanie </option>
                                                    <option value="44">Royaume_Uni </option>
                                                    <option value="7">Russie </option>
                                                    <option value="250">Rwanda </option>
                                                </optgroup>
                                                <optgroup label="S">
                                                    <option value="1758">Sainte_Lucie </option>
                                                    <option value="378">Saint_Marin </option>
                                                    <option value="590">Saint_Martin </option>
                                                    <option value="677">Salomon </option>
                                                    <option value="503">Salvador </option>
                                                    <option value="685">Samoa_Occidentales</option>
                                                    <option value="1684">Samoa_Americaine </option>
                                                    <option value="239">Sao_Tome_et_Principe </option> -->
                                                    <option value="221">Senegal </option>
                                                    <!-- <option value="248">Seychelles </option>
                                                    <option value="232">Sierra Leone </option>
                                                    <option value="65">Singapour </option>
                                                    <option value="421">Slovaquie </option>
                                                    <option value="386">Slovenie</option>
                                                    <option value="252">Somalie </option>
                                                    <option value="249">Soudan </option>
                                                    <option value="94">Sri_Lanka </option>
                                                    <option value="46">Suede </option>
                                                    <option value="41">Suisse </option>
                                                    <option value="597">Surinam </option>
                                                    <option value="268">Swaziland </option>
                                                    <option value="963">Syrie </option>
                                                </optgroup>
                                                <optgroup label="T">
                                                    <option value="992">Tadjikistan </option>
                                                    <option value="886">Taiwan </option>
                                                    <option value="255">Tanzanie </option>
                                                    <option value="235">Tchad </option>
                                                    <option value="66">Thailande </option>
                                                    <option value="670">Timor_Oriental </option>
                                                    <option value="228">Togo </option>
                                                    <option value="1868">Trinite_et_Tobago </option>
                                                    <option value="216">Tunisie </option>
                                                    <option value="993">Turmenistan </option>
                                                    <option value="90">Turquie </option>
                                                    <option value="688">Tuvalu </option>
                                                </optgroup>
                                                <optgroup label="U">
                                                    <option value="380">Ukraine </option>
                                                    <option value="598">Uruguay </option>
                                                </optgroup>
                                                <optgroup label="V">
                                                    <option value="678">Vanuatu </option>
                                                    <option value="379">Vatican </option>
                                                    <option value="58">Venezuela </option>
                                                    <option value="1340">Vierges_Americaines </option>
                                                    <option value="1284">Vierges_Britanniques </option>
                                                    <option value="84">Vietnam </option>
                                                </optgroup>
                                                <optgroup label="W">
                                                    <option value="681">Wallis et Futuma </option>
                                                </optgroup>
                                                <optgroup label="Y">
                                                    <option value="967">Yemen </option>
                                                </optgroup>
                                                <optgroup label="Z">
                                                    <option value="260">Zambie </option>
                                                    <option value="263">Zimbabwe </option>
                                                </optgroup> -->
                                            </select>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <label>Contact</label>
                                            <input type="text" class="form-control input-sm phone" id="phone-mask" name="numero_contact" required="required" data-format="(+999) 99-99-99-99" data-placeholder="Contact">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-sm btn-primary" name="save"><i class="fa fa-user"></i> Créer</button>
                                    <button data-dismiss="modal" class="btn btn-sm btn-default" href="#">Quitter</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>--}}
            <!--  AJOUTER UN NEW CONTACT FIN-->



            <!-- IMPORTATION DE CONTACT DEBUT-->
            <div class="modal fade" id="myModa2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Importer des contacts</h4>
                            <small style="color:red">Vous devez telecharger le fichier d'importation Excel</small>
                        </div>
                        <div class="modal-body">
                            <p>Veillez selectionnez votre fichier excel.</p>
                            <form action="{{ route('addExcelContact') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                <div class="form-group">
                                    <span class="btn btn-primary col-md-12">
                                       <input id="file" name="file" required="required" type="file" class="form-control input-sm">
                                    </span>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                </div>
                                <br/><br/>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-sm btn-primary" name="send"><i class="fa fa-download" aria-hidden="true"></i> Importer</button>
                                    <button data-dismiss="modal" class="btn btn-sm btn-default" href="#">Quitter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--  IMPORTATION DE CONTACT FIN-->
        </div>
        <div class="row">
            <div class="col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                        @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                        @endforeach
                    </div>
                @endif
                <div class="row">
                    <a class="btn btn-success btn-sm" href="{{ URL::asset('/excel/model_d_importation_de_contact.xlsx')}}"><i class="fa fa-upload" aria-hidden="true"></i> Telecharger Le fichier d'Importation Excel </a>
                    <buttom class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModa2"><i class="fa fa-download" aria-hidden="true"></i> Importer</buttom>
                </div>
                <br/>
                <div class="row">
                    <div class="table-responsive" style="margin-top: 40px">
                        <table id="example" class="table table-striped">
                            <thead>
                            <tr>

                                <th>Ref. No.</th>
                                <th>Nom</th>
                                <th>Prénoms</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Fonction</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($contacts as $contact)
                            <tr>
                                <td># 0<?= $i++ ?></td>
                                <td>{{ $contact->nom_contact }}</td>
                                <td>{{ $contact->prenoms_contact }}</td>
                                <td>{{ $contact->email_contact }}</td>
                                <td>{{ $contact->numero_contact }}</td>
                                <td>{{ $contact->fonction_contact }}</td>

                                <td>
                                    <form action="" method="post">
                                        <input name="" value="" type="hidden">
                                        <a class="btn btn-success" href="{{ route('modifcontacts', ['$id' => $contact->id]) }}"><i class="fa fa-edit"> Modifier</i></a>
                                        {{--<a class="btn btn-danger" href=""><i class="fa fa-trash"> Supprimer</i></a>--}}
                                        <a class="btn btn-danger"  onClick="javascript:if(confirm('Voulez-vous réellement supprimer ce contact.')){
                                                window.location = '{{ route('deletecontacts', ['$id' => $contact->id]) }}'
                                                }
                                                else
                                                {
                                                windows.history.back();
                                                }
                                                ;"><i class="fa fa-trash"> Supprimer</i></a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br/><br/>
            </div>
        </div>
    </div>
    @else
    <div class="row col-md-6 col-md-offset-3">
        <div class="alert alert-danger" style="text-align:center">
                        <span class="badge badge-success">
                            <i class="fa fa-info"></i>
                        </span>
            <strong style="text-transform: uppercase;">
                Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
            </strong>
        </div>
    </div>
    @endif
@endsection
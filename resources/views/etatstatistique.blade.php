@extends('layouts.app')

@section('content')
@if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">STATISTIQUE</h4>
        </div>
    </div>
    <div class="row">

    </div>
</div>
@else
<div class="row col-md-6 col-md-offset-3">
    <div class="alert alert-danger" style="text-align:center">
                                            <span class="badge badge-success">
                                                <i class="fa fa-info"></i>
                                            </span>
        <strong style="text-transform: uppercase;">
            Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
        </strong>
    </div>
</div>
@endif
@endsection
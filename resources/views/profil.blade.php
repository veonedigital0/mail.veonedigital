@extends('layouts.app')

@section('content')
@if(Auth::user()->etat_compte != '0')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Mon Profil</h4>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-3 col-sm-3">
                <img src="{{URL::asset('/image/logo.png')}}" alt="" width="280px" height="335px"/>
            </div> -->
            <div class="col-md-12 col-sm-12">

                @if($errors->any())
                    <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                        @foreach($errors->all() as $errorr)
                            {{ $errorr }}<br/>
                        @endforeach
                    </div>
                @endif
            <div class="row">
            <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile" data-toggle="tab">Profil</a>
                </li>
                <li class=""><a href="#editer" data-toggle="tab">Modifier mon profil</a>
                </li>
                <li class=""><a href="#motpass" data-toggle="tab">Changer de mot de passe</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="profile">
                    <!--INFORMATION SUR LE USER-->
                    <div class="page-header">
                        <h4 style="color:green;font-weight:bold">Information personnel</h4>
                    </div>
                    <div class="row">
                        <div class="space"></div>
                        <div class="col-md-9 col-sm-9 col-md-offset-1" >
                            <div>
                                <div class="col-md-6">
                                    <div class="space2"></div>
                                    <p>
                                        <label class="label-text">Nom</label>
                                        <input type="text"class="form-control" value="{{ Auth::user()->name }}" disabled/>
                                    </p>
                                    <p>
                                        <label class="label-text">Contact</label>
                                        <input type="text" class="form-control" value="{{ isset(Auth::user()->contact) ? Auth::user()->contact : '' }}" disabled/>
                                    </p>                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="space2"></div>
                                    <p>
                                        <label class="label-text">Prenoms</label>
                                        <input type="text" class="form-control" value="{{ isset(Auth::user()->prenoms) ? Auth::user()->prenoms : '' }}" disabled/>
                                    </p>
                                    <p>
                                        <label class="label-text">Sexe</label>
                                        <input type="text" class="form-control" value="{{ isset(Auth::user()->sexe) ? Auth::user()->sexe : '' }}" disabled/>
                                    </p>
                                </div>
                                <div class="col-md-12">
                                    <div class="space2"></div>
                                    <p>
                                        <label class="label-text">Login</label>
                                        <input type="text" class="form-control" value="{{ Auth::user()->email }}" disabled/>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--INFORMATION SUR LE USER FIN-->

                    <!--INFORMATION SUR LE COMPTE-->
                    <div class="page-header">
                        <h4 style="color:green;font-weight:bold">Information sur le compte</h4>
                    </div>
                    <div class="row">
                        <div class="space"></div>
                        <div class="col-md-9 col-sm-9 col-md-offset-1" >
                            <div>
                                <div class="col-md-6">
                                    <div class="space2"></div>
                                    <p>
                                        <label class="label-text">Nom du Compte</label>
                                        <input type="text" class="form-control" value="{{ isset(Auth::user()->nom_compte) ? Auth::user()->nom_compte : '' }}" disabled/>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <div class="space2"></div>
                                    <p>
                                        <label class="label-text">Adresse</label>
                                        <input type="text" class="form-control" value="{{ isset(Auth::user()->adresse_compte) ? Auth::user()->adresse_compte : '' }}" disabled/>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--INFORMATION SUR LE COMPTE FIN-->
                </div>
                <div class="tab-pane fade" id="editer">
                    <br/>
                    <h4 style="color:green;font-weight:bold">Modifier mon profil</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('editeUserProfil')}}" method="post" autocomplete="off">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nom">Nom <span class="text-danger">*</span></label>
                                        <input type="text" name="nom" id="nom" class="form-control" value="{{ Auth::user()->name }}" required="required"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="prenom">Prenoms <span class="text-danger">*</span></label>
                                        <input type="text" name="prenom" id="prenom" class="form-control" value="{{ isset(Auth::user()->prenoms) ? Auth::user()->prenoms : '' }}" required="required"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sexe">Sexe <span class="text-danger">*</span></label>
                                        <select class="form-control" name="sexe" id="sexe">
                                            <option value="Homme" <?= Auth::user()->sexe=="Homme" ? "selected" : "" ?>>Homme</option>
                                            <option value="Femme" <?= Auth::user()->sexe=="Femme" ? "selected" : "" ?>>Femme</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adressCompte">Adresse <span class="text-danger">*</span></label>
                                        <input type="text" name="adressCompte" id="adressCompte" class="form-control" value="{{ isset(Auth::user()->adresse_compte) ? Auth::user()->adresse_compte : '' }}" required="required"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ContactCompte">Contact <span class="text-danger">*</span></label>
                                        <input type="text" name="ContactCompte" id="phone-mask" class="form-control input-sm phone" value="{{ isset(Auth::user()->contact) ? Auth::user()->contact : '' }}" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
        <!--                        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>-->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right" name="update"><i class="fa fa-edit"></i> ACTUALISER</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="motpass">
                    <br/>
                    <h4 style="color:green;font-weight:bold">Changer de mot de passe</h4>
                    <div class="space2"></div>
                    <form action="{{ route('editeUserPass')}}" method="post" autocomplete="off">
                        <div class="row col-md-6 col-md-offset-2">
                            <div class="form-group">
                                <input type="password" class="form-control" name="ancienpassword" required="required" placeholder="Mot de passe actuel">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="newpassword" required="required" placeholder="Nouveau mot de passe">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirnewpassword" required="required" placeholder="Confirmer votre mot de passe">
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" class="btn btn-success pull-right" name="passEdit"><i class=" fa fa-refresh"></i> MODIFIER</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
        <br/><br/>
    </div>
@else
<div class="row col-md-6 col-md-offset-3">
    <div class="alert alert-danger" style="text-align:center">
                                                <span class="badge badge-success">
                                                    <i class="fa fa-info"></i>
                                                </span>
        <strong style="text-transform: uppercase;">
            Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
        </strong>
    </div>
</div>
@endif
@endsection
@extends('layouts.app')

@section('content')
@if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">HISTORIQUES DE REHARGEMENT</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if($errors->any())
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    @foreach($errors->all() as $errorr)
                    {{ $errorr }}<br/>
                    @endforeach
                </div>
                @endif
                <div class="text-center alert alert-default col-md-12">
                    <div class="col-md-offset-2">
                        <form action="{{ route('showLogRechar') }}" method="post" autocomplete="off">
                            <div class="col-md-4">
                                <input type="text" name="Date_Debut" id="Date_Debut" class="form-control datepicker" data-format="dd-mm-yyyy" placeholder="Date Debut">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="Date_Fin" id="Date_Fin" class="form-control datepicker" data-format="dd-mm-yyyy" placeholder="Date Fin">
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary" ><i class="fa fa-search"></i> Afficher</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @if(isset($requete))
    <!-- TEST AMUSERMENT -->
        <?php $i=1; ?>
        @foreach($requete as $requet)
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default" style="width: 450px">
                  <div class="panel-body">
                    <div class="col-md-6">
                        <a class="btn btn-sm btn-danger"># 0<?= $i++ ?></a>
                        <h4>{{ $requet->nom_recharge}}</h4>
                        <p class="help-block">{{ $requet->libelle_recharge}}</p>

                    </div>
                    <div class="col-md-6" style="margin-top: 50px">
                        <a href="#" style="text-decoration: none;float: left;padding: 0 20px 0 0;font-size: 11px;color: #000;">
                            <i class="fa fa-calendar"></i>
                            <strong>Date d'édition : </strong> <span style="color: #FF0000">{{ date("d/m/Y",strtotime($requet->created_at))}}</span> 
                        </a>
                        <a href="#" style="text-decoration: none;float: left;padding: 0 20px 0 0;font-size: 11px;color: #000;">
                            <i class="fa fa-comments-o"></i>
                            <strong>Nombre SMS : </strong> <span style="color: #FF0000">{{ $requet->totalsms_recharge}}</span> 
                        </a>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
    <!-- TEST AMUSERMENT -->
   
    </div>
@else
<div class="row col-md-6 col-md-offset-3">
    <div class="alert alert-danger" style="text-align:center">
                                                <span class="badge badge-success">
                                                    <i class="fa fa-info"></i>
                                                </span>
        <strong style="text-transform: uppercase;">
            Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
        </strong>
    </div>
</div>
@endif
@endsection

@extends('layouts.app')
<link href="{{ URL::asset('/css/essentials.css')}}" rel="stylesheet">
@section('content')
    <!-- -->
    <section class="text-center" style="display: block;position: relative;padding: 80px 0;border-bottom: rgba(0,0,0,0.1) 1px solid;transition: all .400s;background-attachment: fixed;background-position: center center;background-repeat: no-repeat;background-size: cover !important;box-sizing: border-box !important;">

        <h1 class="margin-bottom-20" style="font-size: 30px !important;">VEONE DIGITAL || PLATEFORME SMS</h1>
        <!-- <p class="size-20 font-lato">Pour toute demande de compte, veuillez cliquer sur 
            <br><button class="btn btn-success">Crée mon compte</button>
        </p> -->
        <div class="row">
            <p class="size-20 font-lato col-md-6 col-md-offset-3">
                <a href="http://www.veonedigital.ci/" target="_blank" class="btn btn-featured btn-danger">
                    <span>CREER MON COMPTE</span>                    
                </a>
            </p>
        </div>
        <div class="row">
            <!-- socials -->
            <a href="https://web.facebook.com/VeoneDigital/?fref=ts" target="_black" class="social-icon social-icon-sm social-facebook" title="Facebook">
                <i class="fa fa-facebook"></i>
                <i class="fa fa-facebook"></i>
            </a>

            <a href="http://www.veonedigital.ci/" target="_black" class="social-icon social-icon-sm social-ebay" title="Sion-View">
                <i class="fa fa-external-link" aria-hidden="true"></i>
                <i class="fa fa-external-link" aria-hidden="true"></i>
            </a>
            <!-- /socials -->
        </div>
    </section>
    <!-- / -->

    <!-- -->
    <!-- <section class="text-center">
        <div class="container">

            <div class="row">

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-info-circle fa-5x"></i>
                    <h2 class="size-17 margin-top-6">QUI EST SION-VIEW?</h2>
                    <p class="text-muted">Crée en 2010, SION-VIEW est un cabinet d’ingénierie informatique basé à Abidjan en Côte d’ivoire. C’est une structure réactive, spécialisée dans les prestations des Technologies de l’Information et de la Communication.</p>
                </div>

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-clock-o fa-5x"></i>
                    <h2 class="size-17 margin-top-6">QUELS SONT NOS HORAIRES ?</h2>
                    <p class="text-muted">Créer pour vous servir, Sion-view se tient à votre disposition <a>24h/24</a> et <a>7j/7</a>.</p>
                </div>

                <div class="col-md-4 col-sm-4">
                    <i class="fa fa-comments-o fa-5x"></i>
                    <h2 class="size-17 margin-top-6">SUPPORT</h2>
                    <p class="text-muted">Si vous avez des questions, n'hésitez surtout pas à nous écrire sur email suivant <a href="mailto:info@sion-view.com">info@sion-view.com</a> <br/>Contacts: <a>(+225) 22 01 02 86</a> / <a>(+225) 47 60 68 83</a></p>
                </div>

            </div>

        </div>
    </section> -->
    <!-- / -->

    <!-- /wrapper -->

@endsection

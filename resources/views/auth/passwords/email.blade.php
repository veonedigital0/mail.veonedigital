@extends('layouts.app')

<!-- Main Content -->
@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 style="font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid green;text-transform: uppercase;color: green;font-size: 20px;margin-bottom: 40px;">Reintialiser Votre Mot de passe </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-12">Adresse Email</label>

                    <div class="">
                        <input id="email" type="email" class="form-control" name="email" autocomplete="off" required="required" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr/>

                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-success" name="submit" type="submit"><i class="fa fa-btn fa-envelope"></i> &nbsp;Reintialiser Mon Mot de Passe &nbsp;</button>
                    </div>
                </div>
                
            </form>
        </div>
        <div class="col-md-6">   
        <br>                 
            <div class="alert alert-danger">                 
                <ul>
                    <li>
                       Vous devez renseigner votre email. 
                    </li>
                    <li>
                       Un lien de reintialisation vous sera envoyer dans votre Boite Email
                    </li>
                    <li>
                       Et il vous restera qu'à un cliquer sur le lien pour reintialiser votre mot de passe !!
                    </li>
                    <li>
                         Nous vous faisons déjà confiance.
                    </li>
                </ul>
                <br/>                      
            </div>
        </div>
    </div>
</div>
@endsection

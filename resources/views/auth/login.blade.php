@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 style="font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid green;text-transform: uppercase;color: green;font-size: 20px;margin-bottom: 40px;">Veuillez vous Identifiez </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-12 control-label">Adresse Email</label>

                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control" name="email" autocomplete="on" required="required" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-12 control-label">Mot de passe</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control" name="password" autocomplete="off" required="required">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox pull-right">
                            <label>
                                <input type="checkbox" name="remember"> Se souvenir de moi
                            </label>
                        </div>
                    </div>
                </div>

                <br/>
                <hr/>

                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-success" name="submit" type="submit"><span class="glyphicon glyphicon-user"></span> &nbsp;S'identifier &nbsp;</button>
                    </div>
                </div>
                
            </form>
        </div>
        <div class="col-md-6">   
        <br>                 
            <div class="alert alert-danger">                 
                <ul>
                    <li>
                       Vous devez vous identifiez pour acceder à votre espace. 
                    </li>
                    <li>
                       Si vous n’avez pas de compte, veuillez cliquer sur <a href="{{ url('/register') }}"><span style="color:green">Créer un Compte.</span></a>
                    </li>
                    <li>
                       Et cela est facile rien qu'en un clic !!
                    </li>
                    <li>
                         Nous vous faisons déjà confiance.
                    </li>
                </ul>
                <br/>                      
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">CREER MON TEMPLATE</h4>
                </div>
            </div>
            <form action="{{ route('savetemplate') }}" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Description</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="">Nom du template <span class="text-danger">*</span></label>
                                        <input type="text" name="nametemple" id="nametemple" class="form-control" required="required" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Image de la maquette</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4">
                                        {{--<label class="labelUpload">Selectionner une image</label>--}}
                                        <label for="file" class="input input-file">
                                            <div class="button">
                                                Ajouter une image
                                                <input required type="file" accept='image/*' id="file" name="fileTemple" onchange="loadFile(event)">
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-8 col-sm-8">
                                        <div class="thumbnail">
                                            <img id="output" class="img-responsive" src="" alt="Image de la maquette" width="460" height="700" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Inserer le code source</div>
                            <div class="panel-body">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                                <textarea id="ckeditor_full" name="contenu" class="form-control" rows="3" style="width: 100%;height: 50%"></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" name="envoyer" class="btn btn-primary pull-right" style="margin-bottom: 25px"><i class="fa fa-check"></i> ENREGISTRER</button>

                </div>
            </form>
        </div>
    @endif


    <script src="{{ URL::asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };


        var editor_config = {
            height: 500,
            path_absolute : "{{URL::to('/')}}/",
            selector: "#ckeditor_full",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | formatselect fontselect fontsizeselect |  styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | cut copy paste pastetext pasteword | link image media | code | preview",
            menubar: "tools table format view insert edit",
            language : "fr_FR",
            theme : "modern",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);



    </script>

@endsection

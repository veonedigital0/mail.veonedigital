@extends('layouts.app')

@section('content')
@if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">MODIFIER UN CONTACT</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if($errors->any())
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                        @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                        @endforeach
                    </div>
                @endif

                <form role="form" action="{{ route('updatecontacts', ['$id' => $modif->id]) }}" method="post" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nom_contact">Nom du contact <span class="text-danger">*</span></label>
                                <input type="text" id="nom_contact"  class="form-control" name="nom_contact" value="{{ $modif->nom_contact }}" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nom_contact">Email<span class="text-danger">*</span></label>
                                <input type="text" id="email_contact" class="form-control" name="email_contact" value="{{ $modif->email_contact }}" required="required">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prenoms_contact">Pr&eacutenoms du contact <span class="text-danger">*</span></label>
                                <input type="text" id="prenoms_contact"  class="form-control" name="prenoms_contact" value="{{ $modif->prenoms_contact }}" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fonction_contact">Poste du contact</label>
                                <input type="text" id="fonction_contact"  class="form-control" name="fonction_contact" value="{{ $modif->fonction_contact }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="numero_contact">Num&eacutero du contact</label>
                                <input type="text" id="numero_contact" class="form-control" name="numero_contact" value="{{ $modif->numero_contact }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success" name="update"><i class="fa fa-edit"></i> ACTUALISER</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@else
<div class="row col-md-6 col-md-offset-3">
    <div class="alert alert-danger" style="text-align:center">
                                                <span class="badge badge-success">
                                                    <i class="fa fa-info"></i>
                                                </span>
        <strong style="text-transform: uppercase;">
            Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
        </strong>
    </div>
</div>
@endif
@endsection
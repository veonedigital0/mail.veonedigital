@extends('layouts.app')

@section('content')
    @if(!empty($data) or !empty($result))
        @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
            <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">ENVOI DE SMS</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Auth::user()->type_compte !='entreprise')
                  @if(Auth::user()->nbresms_compte == '0')
                    <a href="{{route('addsms')}}" class="btn btn-default col-md-4 col-md-offset-4" style="text-transform: uppercase;"><i class="fa fa-hand-o-right"></i><marquee> Rechargé Mon Compte</marquee></a>
                 @else
                    <form action="{{ route('validdessmsParticulier') }}" method="post" class="col-md-6 col-md-offset-3" >
                        <fieldset>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="">Contacts</label>
                                        <input type="text" name="ContactP" class="form-control" placeholder="Tous les contacts" disabled/>
                                    </div>
                                </div>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="">Message</label>
                                        <textarea rows="5" class="form-control word-count" required="required" maxlength="<?= Auth::user()->type_compte !='entreprise' ? '150' : '450' ?>" data-info="textarea-words-info" placeholder="Message..." name="message" id="message"></textarea>
                                            <span style="padding:6px 10px;background-color:rgba(0,0,0,0.05);margin-top:-2px;border:#ddd 2px solid;border-top-width:1px;display:block;z-index:10;font-size:11px">
                                                <strong>Note:</strong> <?= Auth::user()->type_compte !='entreprise' ? '150' : '450' ?> Caractères maxi!
                                                <span class="pull-right">
                                                    <span id="count"></span> /150 Caractères
                                                </span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="hidden" name="iduser" value="{{ Auth::user()->id }}"/>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                </div>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" name="envoyer" class="btn btn-primary"><i class="fa fa-check"></i> ENVOYER LE MESSAGE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                  @endif
                @else
                    @if(Auth::user()->nbresms_compte == '0')
                        <a href="{{route('addsms')}}" class="btn btn-default col-md-4 col-md-offset-4" style="text-transform: uppercase;"><i class="fa fa-hand-o-right"></i><marquee> Rechargé Mon Compte</marquee></a>
                    @else
                        <form action="{{ route('validdessms') }}" method="post" class="col-md-10 col-md-offset-1" >
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3 text-center alert alert-info">
                                            <label for=""></label>
                                            <p style="font-size:14px;text-align:center;">Selectioner <br/><i>Liste des Groupes</i> <span style="color:red">ET/OU</span> <i>Liste de Contact</i> </p>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="form-group">
                                    @if(!empty($data))
                                        <div class="col-md-6">
                                            <label for="">Liste des Groupes</label>
                                            <select multiple="multiple" class="form-control chzn-select pull-left" name="groupe[]">
                                                @foreach($data as $data)
                                                <option name="id_groupe" value="{{ $data->id }}">{{ $data->nom_groupe }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    @if(!empty($result))
                                        <div class="col-md-6">
                                            <label for="">Liste de Contact</label>
                                            <select multiple class="form-control chzn-select" name="contact[]">
                                                @foreach($result as $result)
                                                <option name="id_contact" value="{{ $result->id }}">{{ $result->nom_contact }} {{ $result->prenoms_contact }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    </div>
                                </div>

                                <br/>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="">Message</label>
                                            <textarea rows="5" class="form-control word-count" required="required" maxlength="450" data-info="textarea-words-info" placeholder="Message..." name="message" id="message"></textarea>
                                                <span style="padding:6px 10px;background-color:rgba(0,0,0,0.05);margin-top:-2px;border:#ddd 2px solid;border-top-width:1px;display:block;z-index:10;font-size:11px">
                                                    <strong>Note:</strong> 450 Caractères maxi!
                                                    <span class="pull-right">
                                                        <span id="count"></span> /150 Caractères
                                                    </span>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" name="envoyer" class="btn btn-primary"><i class="fa fa-check"></i> ENVOYER LE MESSAGE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif
                @endif

            </div>
        </div>
    </div>
        @else
            <div class="row col-md-6 col-md-offset-3">
                <div class="alert alert-danger" style="text-align:center">
                    <span class="badge badge-success">
                        <i class="fa fa-info"></i>
                    </span>
                    <strong style="text-transform: uppercase;">
                        Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
                    </strong>
                </div>
            </div>
        @endif
    @else
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">ENVOI DE SMS</h4>
                    </div>
                </div>
                <div class="row col-md-6 col-md-offset-3">
                    <div class="alert alert-danger" style="text-align:center">
                        <span class="badge badge-success">
                            <i class="fa fa-info"></i>
                        </span>
                        <strong style="text-transform: uppercase;">
                            Désolé ! vous devez ajouter des contacts pour envoyer des messages
                        </strong>
                    </div>
                </div>
            </div>
    @endif
@endsection

@extends('layouts.app')

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
            <div class="row">
                <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Ajout de contact au groupe : <span style="color:green;border-bottom: 2px solid #e7e7e7;margin-bottom:40px;">{{$groupename}}</span></h4>
            </div>
        </div>
        <br/><br/>
        <div class="container">
     @if(!empty($data))
	    <div class="row">
	        <form action="{{ route('addAppartenir') }}" method="post" class="col-md-8 col-md-offset-2 jumbotron form-group">
                @if($errors->any())
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    @foreach($errors->all() as $errorr)
                    {{ $errorr }}<br/>
                    @endforeach
                </div>
                @endif

                <div class="form-group">
	                <label class="control-label">Selectionner un Contact</label>
	                <div class="col-md-12" style="margin-bottom: 20px;">
	                    <select multiple class="form-control chzn-select input-lg" name="IdContact[]">
	                        @foreach($data as $contact)
	                        <option value="{{ $contact->id }}">{{ $contact->nom_contact }} {{ $contact->prenoms_contact }}</option>
	                        @endforeach
	                    </select>

	                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
	                    <input type="hidden" name="IdGroup" value="{{ $id }}"/>
	                </div><!-- /.col -->
	            </div><!-- /form-group -->

	            <button type="submit" class="btn btn-success pull-right" name="addContGpe"><i class="fa fa-save"></i> AJOUTER</button>
	        </form>
	    </div>
     @else
        <div class="row col-md-6 col-md-offset-3">
            <div class="alert alert-danger" style="text-align:center">
                <span class="badge badge-success">
                    <i class="fa fa-info"></i>
                </span>
                <strong style="text-transform: uppercase;">
                    Désolé ! vous devez ajouter des contacts
                </strong>
            </div>
            <div class="text-center">
                <a class="btn btn-default btn-sm" href="{{ route('gestioncontacts') }}">Ajouter Des Contacts !</a>
            </div>

        </div>
     @endif
	</div>
    @else
        <div class="row col-md-6 col-md-offset-3">
            <div class="alert alert-danger" style="text-align:center">
                                            <span class="badge badge-success">
                                                <i class="fa fa-info"></i>
                                            </span>
                <strong style="text-transform: uppercase;">
                    Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
                </strong>
            </div>
        </div>
    @endif
@endsection
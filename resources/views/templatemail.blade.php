@extends('layouts.app')

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a class="btn btn-primary btn-sm" href="{{route('create-template')}}"><i class="fa fa-comment"> Créer un template</i></a>
                    </div>
                    <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">MES TEMPLATES</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 4%;">
                        <div class="thumbnail" style="height: 270px;overflow: hidden;">
                            <img src="templates/vierge.png" alt="nomimg">
                        </div>
                        <div class="caption">
                            <h4>Ecrire mon mail</h4>
                            <p>
                                <a href="templates/vierge.png" class="btn btn-primary btn-sm" role="button" title="voir"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('mail', ['$id' => 'a']) }}" class="btn btn-default btn-sm" role="button"><i class="fa fa-check"></i></a>
                            </p>
                        </div>
                    </div>
                    @if(isset($temps)&& !empty($temps))
                        @foreach($temps as $item)

                            @php
                                $urlTempl = $item->img;
                                if ($item->auteur == '1'){ $urlTempl='http://adminewsletters.veonedigital.com/'.$item->img ; }
                            @endphp

                            <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 4%;">
                                <div class="thumbnail" style="height: 270px;overflow: hidden;">
                                    <img src="{{$urlTempl}}" alt="{{$item->nom}}">
                                </div>
                                <div class="caption">
                                    <h4>{{ucfirst($item->nom)}}</h4>
                                    <p>
                                        <a href="{{$urlTempl}}" class="btn btn-primary btn-sm" role="button" target="_blank"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('editmail', ['$id' =>$item->id ]) }}" class="btn btn-danger btn-sm" role="button"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('mail', ['$id' =>$item->id ]) }}" class="btn btn-success btn-sm" role="button"><i class="fa fa-check"></i></a>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endif


@endsection

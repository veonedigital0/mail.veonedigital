<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plateforme MAIL</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    {{--<!--    <link href="{{ URL::asset('/js/bootstrap.min.css')}}" rel="stylesheet">-->--}}

    <!-- Font Awesome -->
    {{--<link href="{{ URL::asset('/js/font-awesome.min.css')}}" rel="stylesheet">--}}

    <!-- Pace -->
    <link href="{{ URL::asset('/css/pace.css')}}" rel="stylesheet">

    <!-- Chosen -->
    <link href="{{ URL::asset('/css/chosen/chosen.min.css')}}" rel="stylesheet"/>

    <!-- Datepicker -->
    <link href="{{ URL::asset('/css/datepiker.css')}}" rel="stylesheet"/>

    <!-- Timepicker -->
    <link href="{{ URL::asset('/css/timepiker.min.css')}}" rel="stylesheet"/>

    <!-- Slider -->
    <link href="{{ URL::asset('/css/slider.css')}}" rel="stylesheet"/>

    <!-- Tag input -->
    <link href="{{ URL::asset('/css/tagsinput.css')}}" rel="stylesheet"/>

    <!-- WYSIHTML5 -->
    <link href="{{ URL::asset('/css/bootstrap-wysihtml5.css')}}" rel="stylesheet"/>

    <!-- Dropzone -->
    <link href="{{ URL::asset('/css/dropzone/dropzone.css')}}" rel="stylesheet"/>


    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .mce-combobox .mce-btn{
            display: none;
        }
    </style>
</head>

<body id="app-layout">

<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->


            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('campagne') }}">
                <i class="fa fa-desktop" aria-hidden="true" style="font-size: 30px;"></i>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->

            <ul class="nav navbar-nav">
                @if (!Auth::guest())

                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Gestion des utilisateurs</a></li>--}}
                            {{--<li role="separator" class="divider"></li>--}}
                            {{--<li><a href="{{ route('gestiondescomptes') }}">Gestion des comptes</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                @if(Auth::user()->etat_compte == '0' or Auth::user()->etat_compte == '2')
                <script>
                    alert('Désolé, votre compte est inactif ou inexistant ! veuillez contacter l\'administrateur ');
                </script>
                @endif

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp; Enregistrement <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('gestioncontacts') }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; Gestion des Contacts</a></li>

                            @if(Auth::user()->type_compte =='entreprise')
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('gestiondesgroupes') }}"><i class="fa fa-users" aria-hidden="true"></i>&nbsp; Gestion des Groupes</a>
                            </li>
                            @endif

                            {{--<li><a href="{{ route('gestiondessms') }}">Envoi des SMS</a></li>--}}

                        </ul>
                    </li>

                    {{--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-arrows-alt" aria-hidden="true"></i>&nbsp;&nbsp; Exploitation <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('logrechargement') }}"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp; Historique Rechargement</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('historique') }}"><i class="fa fa-history" aria-hidden="true"></i>&nbsp; Historiques SMS</a></li>

                            <!-- <li role="separator" class="divider"></li>
                            <li><a href="{{ route('etatstatistique') }}"><i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp; Statistiques</a></li> -->
                            
                        </ul>
                    </li>--}}

                    {{--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp; Envoie <span class="caret"></span></a>
                        <ul class="dropdown-menu">
--}}{{--                            <li><a href="{{ route('gestiondessms') }}"><i class="fa fa-comment" aria-hidden="true"></i>  Envoyer SMS</a></li>--}}{{--
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('campagne') }}"><i class="fa fa-envelope-square" aria-hidden="true"></i>  Envoyer Mail</a></li>
                        </ul>
                    </li>--}}
                    {{--<li><a href="{{ route('mytemplates') }}"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp; Templates</a></li>--}}

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp; Gestion des templates <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('mytemplates') }}"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp; Mes Templates</a></li>
                                <li role="separator" class="divider"></li>
                            <li><a href="{{ route('create-template') }}"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Créer un template</a> </li>
                        </ul>
                    </li>

                    <li><a href="{{ route('campagne') }}" style="color: #2cfb09;"><i class="fa fa-envelope-square" aria-hidden="true"></i>&nbsp;&nbsp; Envoyer Mail</a></li>
                @endif
            </ul>

            {{--</ul>
                 --}}{{--<li><a href="{{ url('/home') }}">Home</a></li>
                 <li><a href="{{ route('administration') }}">Administration</a></li>
                 <li><a href="{{ route('enregistrement') }}">Enregistrement</a></li>
                 <li><a href="{{ route('exploitation') }}">Exploitation</a></li>--}}{{--
             </ul>--}}

            <!-- Right Side Of Navbar -->

            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->

                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Connexion</a></li>
                    {{--<li><a href="{{ url('/register') }}"><i class="fa fa-user" aria-hidden="true"></i> Créer un Compte</a></li>--}}

                @else
                    <li>
                        {{--<a href="{{ url('/') }}"><span style="color:#FFF">MAIL</span> <span class="badge" style="background: green">{{ Auth::user()->nbresms_compte }}</span></a>--}}

                        {{--<a href="{{ route('addsms') }}"><span style="color:#FFF">MAIL</span> <span class="badge" style="background: green">{{ Auth::user()->nbresms_compte }}</span></a>--}}
<!--                        style="background: green !important;top:8px !important;right: -6px !important;position: absolute !important;"-->
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user hidden-xs"></i>&nbsp;{{ Auth::user()->nom_compte }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('profil') }}"><i class="fa fa-eye hidden-xs"></i> Profil</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i> D&eacuteconnexion</a></li>
                        </ul>

                    </li>

                @endif
            </ul>
        </div>
    </div>
</nav>
<!--AFFICHER UN SET_FLASH DEMANDER A LUSER DE CHANGER SON MDP-->
@if (!Auth::guest())
@if(Auth::user()->secure == '0')
<div class="container">
        <div class="alert alert-danger alert-dismissable col-md-7 col-md-offset-3" role="alert" style="
    text-align:center;">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
            <span class="badge badge-success"><i class="fa fa-info"></i></span>
            <strong style="text-transform: uppercase;"> Pour des raisons de sécurité veuillez changer votre mot de passe.</strong>
            <br/><a href="{{ route('profil') }}"><i class="fa fa-hand-o-right"></i> Changer Mon Mot de Passe</a>
        </div>
</div>
@endif
@endif
<!--AFFICHER UN SET_FLASH DEMANDER A LUSER DE CHANGER SON MDP-->


<!--AFFICHER UN SET_FLASH DE SUCCESS-->
@if(Session::has('success'))
    <div class="container">
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
            {{Session::get('success')}}
        </div>
    </div>
    @endif
            <!--AFFICHER UN SET_FLASH DE SUCCESS-->

    <!--AFFICHER UN SET_FLASH DE ERRORS-->
    @if(Session::has('error'))
        <div class="container">
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                {{Session::get('error')}}
            </div>
        </div>
        @endif
                <!--AFFICHER UN SET_FLASH DE ERROS-->


        <br><br>
        @yield('content')

                <!-- JavaScripts -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        {{--<script src="{{ elixir('js/app.js') }}"></script>--}}

        {{--<!-- Jquery -->
<!--        <script scr="{{ URL::asset('/js/jquery-1.10.2.min.js')}}"></script>-->

        <!-- Bootstrap -->
        <script src="{{ URL::asset('/js/bootstrap.min.js')}}"></script>-->
--}}
        <!-- Chosen -->
        <script src="{{ URL::asset('/js/chosen.jquery.min.js')}}"></script>

        <script type="text/javascript" src="{!! asset('/js/js_pay_script.js') !!}"></script>

        <!-- Mask-input -->
        <script src="{{ URL::asset('/js/jquery.maskedinput.min.js')}}"></script>

        <!-- Datepicker -->
        <script src="{{ URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>

        <!-- Timepicker -->
       <script src="{{ URL::asset('/js/bootstrap-timepicker.min.js')}}"></script>

        <!-- Slider -->
        <script src="{{ URL::asset('/js/bootstrap-slider.min.js')}}"></script>

        <!-- Tag input -->
        <script src="{{ URL::asset('/js/jquery.tagsinput.min.js')}}"></script>

        <!-- WYSIHTML5 -->
        <script src="{{ URL::asset('/js/wysihtml5-0.3.0.min.js')}}"></script>
        <script src="{{ URL::asset('/js/uncompressed/bootstrap-wysihtml5.js')}}"></script>

        <!-- Dropzone -->
        <script src="{{ URL::asset('/js/dropzone.min.js')}}"></script>

        <!-- Modernizr -->
        <script src="{{ URL::asset('/js/modernizr.min.js')}}"></script>

        <!-- Pace -->
        <script src="{{ URL::asset('/js/pace.min.js')}}"></script>

        <!-- Popup Overlay -->
        <script src="{{ URL::asset('/js/jquery.popupoverlay.min.js')}}"></script>

        <!-- Slimscroll -->
        <script src="{{ URL::asset('/js/jquery.slimscroll.min.js')}}"></script>

        <!-- Cookie -->
        <script src="{{ URL::asset('/js/jquery.cookie.min.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

        <!-- Endless -->
        <script src="{{ URL::asset('/js/endless/endless_form.js')}}"></script>
        <script src="{{ URL::asset('/js/endless/endless.js')}}"></script>

        <script>
        $(document).ready(function() {
        count($("#message"),$("#count"));
        $("#message").keyup(function() {
        count($("#message"),$("#count"));
        });
        });

        function count(src,dest){
        var txtVal = src.val();
        var words = txtVal.trim().replace(/\s+/gi, ' ').split(' ').length;
        var chars = txtVal.length;
        if(chars===0){words=0;}
        dest.html(chars);
        }
        
        // $('textarea[maxlength]').keyup(function(){
        //     //get the limit from maxlength attribute
        //     var limit = parseInt($(this).attr('maxlength'));
        //     //get the current text inside the textarea
        //     var text = $(this).val();
        //     //count the number of characters in the text
        //     var chars = text.length;

        //     //check if there are more characters then allowed
        //     if(chars > limit){
        //         //and if there are use substr to get the text before the limit
        //         var new_text = text.substr(0, limit);

        //         //and change the current text with the new text
        //         $(this).val(new_text);
        //     }
        // });

        </script>
</body>
</html>

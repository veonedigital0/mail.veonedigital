@extends('layouts.app')

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
            <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;color:green;text-transform: uppercase;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Modification d'un Groupe de Contact</h4>
        </div>
        <br/><br/>
        <div class="row">
        <form action="{{ route('updategroupes',['id'=>$resultat->id]) }}" method="post" autocomplete="off" class="col-md-4 col-md-offset-4 jumbotron form-group">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                @foreach($errors->all() as $errorr)
                {{ $errorr }}<br/>
                @endforeach
            </div>
            @endif
            <p>
                <label for="nom_groupe">Nom du Groupe</label>
                <input type="text" name="nom_groupe" id="nom_groupe" value="{{ $resultat->nom_groupe }}" class="form-control"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type="hidden" name="id" value="{{ $resultat->id }}"/>
            </p>
            <br/><br/>
            <button type="submit" class="btn btn-success pull-right" name="updateGpe"><i class="fa fa-edit"></i> ACTUALISER</button>
        </form>
    </div>
    @else
        <div class="row col-md-6 col-md-offset-3">
            <div class="alert alert-danger" style="text-align:center">
                                            <span class="badge badge-success">
                                                <i class="fa fa-info"></i>
                                            </span>
                <strong style="text-transform: uppercase;">
                    Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
                </strong>
            </div>
        </div>
    @endif

@endsection
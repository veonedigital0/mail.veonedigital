
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">ESPACE DE RECHARGEMENT SMS</h4>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="container">
    <div class="row">

    <!-- LEFT -->
    <div class="col-md-5 col-sm-5">

        <div class="heading-title heading-border-bottom heading-color">
            <h3><span>DERNIER RECHARGEMENT</h3>
            <br/>
        </div>

        <div class="margin-bottom-80">
          @if(isset($recharge) && !empty($recharge))
            <h5>VOS DERNIER RECHARGEMENT ?</h5>
            <ul class="list-unstyled list-icons">
                @foreach($recharge as $recharge)
                    <li><i class="fa fa-plus-square"></i>{{ $recharge->libelle_recharge}} | {{ $recharge->totalsms_recharge}} SMS | {{ date("d/m/Y",strtotime($recharge->created_at))}} </li>
                @endforeach
            </ul>
          @else
            <h5>VOUS N'AVEZ PAS EFFECTUER DE RECHARGEMENT POUR L'INSTANT !</h5>
          @endif
        </div>

    </div>

    <!-- RIGHT - FORM -->
    <div class="col-md-7 col-sm-7">

        <div class="heading-title heading-border-bottom">
            <h3>FORMULAIRE DE RECHARGEMENT</h3>
            <br/>
        </div>


        <form class="form-group" method="post" id="rechargement">
            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6">
                            <label>Nom</label>
                            <input value="<?=Auth::user()->name?>" class="form-control required" type="text" disabled>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label>Prenoms</label>
                            <input disabled value="<?=Auth::user()->prenoms?>"  class="form-control required" type="text">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6">
                            <label>Email</label>
                            <input disabled value="<?=Auth::user()->email?>" class="form-control required" type="email">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label>Contact</label>
                            <input readonly="readonly" value="<?=Auth::user()->contact?>" class="form-control required" type="text">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12">
                            <label>Montant du Rechargement *</label>
                            <input name="montant_a_payer" id="montant_a_payer" class="form-control required" type="number" min="35" required="" placeholder="10000 FCFA">
                            <p class="help-block">Noter que pour 35 FCFA vous avez 1 SMS</p> 
                        </div>
                    </div>
                </div>

            </fieldset>
            <br/>
            <div class="row">
                <div class="col-md-3">                    
                    <button type="button" class="btn btn-success pull-left" name="valider" id="valider"><i class="fa fa-save"></i> RECHARGER</button>
                </div>
                
            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="nom" id="nom" value="{{ Auth::user()->id }}"/>
            <input type="hidden" name="trans_id" id="trans_id" value="<?= mt_rand() ?>"/>
            <input type="hidden" name="date_transaction" id="date_transaction"  value="<?= date("Y-m-d H:i:s") ?>"/>
            <input type="hidden" name="type_pay" id="type_pay" value="rechargement"/>
            <input type="hidden" name="numero" id="numero" value="<?=Auth::user()->contact?>"/>

        </form>
        <div class="row col-md-offset-3">
        <div id="sion_loader" class="col-md-6" style="display: none;float:left">
            <img src="{{URL::asset('image/ajax-loader.gif')}}" width="31px" height="31px">
        </div>
        </div>

        <div id="back_message">            
        </div>

        <hr class="margin-top-60">
        <div class="col-md-6">
            <div class="text-center margin-top-60">
                <i class="fa fa-phone fa-3x"></i>
                <h2 class="font-raleway nomargin">(+225) 20 00 46 60</h2>
                <span class="size-13 text-muted">24H/24 et 7J/7</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="text-center margin-top-60">
                <i class="fa fa-envelope fa-3x"></i>
                <h2 class="font-raleway nomargin">info@veonedigital.ci</h2>
                <span class="size-13 text-muted">24H/24 et 7J/7</span>
            </div>
        </div>


    </div>

    </div>
    <br><br> 
 </div>
<!-- PAY SCRIPT -->
<!-- <script src="{{ URL::asset('/js/js_pay_script.js')}}"></script> -->

@endsection
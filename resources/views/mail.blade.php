@extends('layouts.app')

@section('content')
    @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
        <div class="container">
            @if($errors->any())
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <h4 style="margin-top:-30px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">ENVOI DE MAIL</h4>
                </div>
            </div>
        <form action="{{ route('sendmail') }}" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-success">
                        <div class="panel-heading"><i class="fa fa-mail"></i> Campagne</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="nom_contact">Nom de la campagne <span class="text-danger">*</span></label>
                                <input type="text" id="campagname" class="form-control" name="campagname" required="required" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Sélection des destinataires</div>
                        <div class="panel-body">
                            <div class="form-group">
                                @if(!empty($data))
                                    <div class="col-md-6">
                                        <label for="">Liste des Groupes</label>
                                        <select multiple="multiple" class="form-control chzn-select pull-left" name="groupe[]">
                                            @foreach($data as $data)
                                                <option name="id_groupe" value="{{ $data->id }}">{{ $data->nom_groupe }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                @if(!empty($result))
                                    <div class="col-md-6">
                                        <label for="">Liste de Contact</label>
                                        <select multiple class="form-control chzn-select" name="contact[]">
                                            @foreach($result as $result)
                                                <option name="id_contact" value="{{ $result->email_contact }}">{{ $result->nom_contact }} {{ $result->prenoms_contact }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Renseignez</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="nom_contact">Subject <span class="text-danger">*</span></label>
                                <input type="text" id="sujet" class="form-control" name="sujet" required="required" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><i class="fa fa-hand-o-right"></i> Rédiger votre mail</div>
                        <div class="panel-body">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <input type="hidden" name="iduser" value="{{ Auth::user()->id }}"/>
                            <input type="hidden" name="idtemp" value="{{ $idtemp }}"/>
                            <textarea id="ckeditor_full" name="contenu" class="form-control" rows="3" style="width: 100%;height: 50%">{{$source}}</textarea>
                        </div>
                    </div>

                    <button type="submit" name="envoyer" class="btn btn-primary" style="margin-bottom: 25px"><i class="fa fa-check"></i> ENVOYER LE MAIL</button>
                </div>
            </div>
        </form>
        </div>
    @endif


    <script src="{{ URL::asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        var editor_config = {
            height: 500,
            path_absolute : "{{URL::to('/')}}/",
            selector: "#ckeditor_full",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | formatselect fontselect fontsizeselect |  styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | cut copy paste pastetext pasteword | link image media | code | preview",
            menubar: "tools table format view insert edit",
            language : "fr_FR",
            theme : "modern",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>

@endsection

@extends('layouts.app')

@section('content')
    @if(Auth::user()->type_compte !='entreprise')

        <div class="container">
            <div class="row">
                <div class="alert alert-danger margin-bottom-30" style="font-size:20px">
                    <h4><strong>Oouup! Il me semble qu'il a un problème</strong></h4>
                    <p style="color: #000">Vous n'avez pas droit à cette page. Pour plus d'information veuillez contact l'administrateur de l'application</p>
                </div>
            </div>
        </div>
    @else
        @if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
            <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <buttom class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-user"></i> Ajouté un Groupe
                    </buttom>
                </div>
                <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;color: GREEN;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Liste des Groupes</h4>
            </div>
            <!--                AJOUTER UN NEW GROUPE DEBUT-->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Nouveau Groupe</h4>
                        </div>
                        <form action="{{ route('validgroupes') }}" method="post" autocomplete="off">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control input-sm" id="nom" name="nom_groupe" placeholder="Group ..." required="required" type="text">
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="iduser" value="{{ Auth::user()->id }}"/>
                            </div>
                            <br>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" href="#">Quitter</button>
                                <button type="submit" class="btn btn-primary" name="save"><i class="fa fa-users"></i> Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--                AJOUTER UN NEW GROUPE FIN-->
        </div>
        <div class="row">
            <div class="col-md-12">

                @if($errors->any())
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                        @foreach($errors->all() as $errorr)
                        {{ $errorr }}<br/>
                        @endforeach
                    </div>
                @endif

                <br/>
                <div class="row">
                    <div class="table-responsive table-bordered">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Ref. No.</th>
                                <th>Nom Du Groupe</th>
                                <th>Nombre de Contact</th>
                                <th>Date Creation Du groupe</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                    <?php $i=1; ?>
                    @foreach($groups as $group)
                            <tr>
                                <td># 0<?= $i++ ?></td>
                                <td>{{ $group->nom_groupe}}</td>
                                <?php $nbCont = DB::table('appartenir')->where('groupe_id',$group->id)->count();?>
                                <td>{{$nbCont}}</td>
                                <td>{{ date("d/m/Y",strtotime($group->created_at))}}</td>
                                <td>
                                    <form action="" method="post">
                                        <input name="" value="" type="hidden">
                                        <a class="btn btn-default" href="{{ route('viewgroup',['id'=>$group->id]) }}"><i class="fa fa-eye"> View</i></a>
                                        <a class="btn btn-warning" href="{{ route('addContactgroup',['id'=>$group->id]) }}"><i class="fa fa-users"> Ajouté</i></a>
                                        <a class="btn btn-success" href="{{ route('editgroup',['id'=>$group->id]) }}"><i class="fa fa-edit"> Modifier</i></a>
                                        <a class="btn btn-danger"  onClick="javascript:if(confirm('Avant la suppression rassurer vous que cet groupe ne contient pas de contacts')){
                                                window.location = '{{ route('deletegroup',['id'=>$group->id]) }}'
                                                }
                                                else
                                                {
                                                windows.history.back();
                                                }
                                                ;"><i class="fa fa-trash"> Supprimer</i></a>
                                    </form>
                                </td>
                            </tr>
                    @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @else
            <div class="row col-md-6 col-md-offset-3">
                <div class="alert alert-danger" style="text-align:center">
                                    <span class="badge badge-success">
                                        <i class="fa fa-info"></i>
                                    </span>
                    <strong style="text-transform: uppercase;">
                        Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
                    </strong>
                </div>
            </div>
        @endif

    @endif

@endsection
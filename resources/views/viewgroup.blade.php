@extends('layouts.app')

@section('content')
@if(Auth::user()->etat_compte != '0' AND Auth::user()->etat_compte != '2')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-top:-8px;font-weight: 900;padding-bottom: 20px;border-bottom: 2px solid GREEN;text-transform: uppercase;font-size: 20px;margin-bottom: 40px;" class="page-head-line">Groupe <span style="color:green;border-bottom: 2px solid #e7e7e7;margin-bottom:40px;">{{$group}}</span> </h4>
        </div>
    </div>
    <br/>
    @if(!empty($contacts))

        <div class="row">
            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Ref. No.</th>
                        <th>Nom </th>
                        <th>Prenoms</th>
                        <th>Email</th>
                        <th>Numero</th>
                        <th>Fonction</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($contacts as $contact)
                    <tr>
                        <td># 0<?= $i++ ?></td>
                        <td>{{ $contact->nom_contact}}</td>
                        <td>{{ $contact->prenoms_contact}}</td>
                        <td>{{ $contact->email_contact}}</td>
                        <td>{{ $contact->numero_contact}}</td>
                        <td>{{ $contact->fonction_contact}}</td>
                        <td>
                            <form action="{{ route('deleteContactGroup') }}" method="post">
                                <input name="idContact" value="{{ $contact->id}}" type="hidden">
                                <input name="idGroup" value="{{ $id}}" type="hidden">
                                <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                <button class="btn btn-danger" type="submit" name="del"><i class="fa fa-trash"> Supprimer</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else

        <div class="row col-md-6 col-md-offset-3">
            <div class="alert alert-danger">
                <span class="badge badge-success">
                    <i class="fa fa-info"></i>
                </span>
                <strong style="text-transform: uppercase;">
                    Désolé Aucun Contact n'est present dans ce groupe
                </strong>
            </div>
            <div class="text-center">
                <a class="btn btn-default btn-sm" href="{{ route('addContactgroup',['id'=>$id]) }}">Ajouter Des Contacts Aux Groupes !</a>
            </div>

        </div>

    @endif

</div>
@else
<div class="row col-md-6 col-md-offset-3">
    <div class="alert alert-danger" style="text-align:center">
                                                <span class="badge badge-success">
                                                    <i class="fa fa-info"></i>
                                                </span>
        <strong style="text-transform: uppercase;">
            Désolé, votre compte est inactif ou inexistant ! veuiller contacter l'administrateur
        </strong>
    </div>
</div>
@endif
@endsection
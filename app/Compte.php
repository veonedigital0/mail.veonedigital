<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Compte extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'compte';

    protected $fillable = [
        'nom_compte','nombre_sms',
    ];


}

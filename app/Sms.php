<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'sms';

    protected $fillable = ['message','status-msg'];


}

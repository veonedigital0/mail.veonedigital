<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'contact';

    protected $fillable = [
        'nom_contact', 'prenoms_contact','numero_contact','libelle_contact',
    ];

    public function groupe()
    {
        return $this->belongsToMany('smsgestion\Groupe');
    }



}

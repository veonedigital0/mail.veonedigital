<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Type_user extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'type_user';

    protected $fillable = [
        'libelle_type',
    ];

}

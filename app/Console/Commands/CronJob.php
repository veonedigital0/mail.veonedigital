<?php

namespace smsgestion\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use smsgestion\Sendmail;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:cronjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changer le nom du premier user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*\DB::table('users')
            ->where('id',10)
            ->update(['name'=>str_random(10)]);*/
        $all2 = Sendmail::orderBy('id', 'asc')->take(50)->get();
        if($all2){
            $data=array(
                "textBody"=>$all2[0]->src,
                "sujets"=>$all2[0]->sujet
            );
            //$emailUser = \Auth::user()->email;

            foreach($all2 as $itemMail){
                $email = $itemMail->email;
                $sujet = $itemMail->sujet;
                $emailuser = $itemMail->emailuser;

                Mail::send('mail/email',$data,function($message) use($sujet,$email,$emailuser){
                    $message->to($email);
                    $message->from($emailuser);
                    $message->subject($sujet);
                });

                $itemMailDel = Sendmail::findOrFail($itemMail->id);
                $itemMailDel->delete();
            }


        }

        $this->info("le nom de l'user a ete changer ");
    }
}

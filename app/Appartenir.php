<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Appartenir extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'appartenir';

    protected $fillable = [
        'nombre_contact',
    ];

    public function groupe()
    {
        return $this->hasMany('smsgestion\Groupe');
    }

    public function contact()
    {
        return $this->hasMany('smsgestion\Contact');
    }

}

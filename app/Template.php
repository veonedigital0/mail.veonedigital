<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public $timestamps = false;
}

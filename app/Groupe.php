<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Groupe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'groupe';

    protected $fillable = [
        'nom_groupe',
    ];

    public function contact()
    {
        return $this->belongsToMany('smsgestion\Contact');
    }

}

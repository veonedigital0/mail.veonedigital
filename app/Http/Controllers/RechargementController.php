<?php

namespace smsgestion\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use smsgestion\Contact;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;
use smsgestion\Log_rechargement;

class RechargementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function logrechargement()
    {
        return view('logrechargement');
    }

    public function showLogRechar(Request $request)
    {
        $resulte = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'Date_Debut' =>'required|date',
            'Date_Fin' =>'required|date|after:Date_Debut',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $dteD = explode('-', substr($resulte['Date_Debut'],0,10));
            $dteF = explode('-', substr($resulte['Date_Fin'],0,10));

            $dteDebut = $dteD[2].'-'.$dteD[1].'-'.$dteD[0];
            $dteFin = $dteF[2].'-'.$dteF[1].'-'.$dteF[0];
            //$tab = array();
            $requete = DB::table('log_rechargements')
                            ->where('id_user',Auth::user()->id)
                            ->whereBetween('created_at',[$dteDebut,$dteFin])
                            ->orderBy('created_at', 'desc')
                            ->get();
            //$tab[]=$requete;
            //dd($tab);
            if(isset($requete)&& !empty($requete))
            {
                return view('logrechargement')->with('requete',$requete);
                //dd($requete);
            }else{
                return redirect()->route('logrechargement')->with('error','Désolé aucun resultat pour votre recherche!');
            }
        }



    }

}
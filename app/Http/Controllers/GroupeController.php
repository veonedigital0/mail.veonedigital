<?php

namespace smsgestion\Http\Controllers;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use smsgestion\Appartenir;
use smsgestion\Groupe;
use smsgestion\Contact;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;

class GroupeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        return url('/login');
    }

    public function gestiondesgroupes()
    {
        $groups = DB::table('groupe')->where('user_id', Auth::user()->id)->orderBy('nom_groupe','asc')->get();


        return view('gestiondesgroupes')->with('groups',$groups);
//        return view('gestiondesgroupes')->with('$contact',$contact);
    }

    public function editgroup($id)
    {
        $verif = DB::table('groupe')->select('user_id')->where('id',$id)->first();

        if ($verif->user_id != Auth::user()->id) {
            return redirect()->route('gestiondesgroupes')->with('error','Désolé ! l\'identifiant du groupe saisir n\'est dans vos données!');

        }else{
            $resultat = Groupe::find($id);
            if($resultat){
                return view('editgroup')->with('resultat',$resultat);
            }else{
                return redirect() -> route('gestiondesgroupes')->with('error','Desoler modification impossible !');
            }
        }

        

    }

    public function validgroupes(Request $request)
    {
        $reponse= $request -> except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'nom_groupe' =>'max:255|min:3|required'
        ]);

        if($valider->fails()){
            return redirect()->route('gestiondesgroupes')->withErrors($valider->errors());
        }else{
            $nomGroup =  $reponse['nom_groupe'];
            $libelle_exit = Groupe::where('nom_groupe', $nomGroup)->first();

            if(!$libelle_exit)
            {
                $groupe = new Groupe();
                $groupe->nom_groupe = $reponse['nom_groupe'];
                $groupe->user_id = $reponse['iduser'];
                $groupe->save();

                return redirect() -> route('gestiondesgroupes')->with('success','Félicitation Groupe Créer !');
            }
            return redirect() -> route('gestiondesgroupes')->with('error','Désolé le Groupe Existe Déjà !');
        }




    }

    public function updategroupes(Request $request, $id)
    {

        $data= $request -> except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom_groupe' =>'required|max:225|min:3'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $group = Groupe::find($id);
            //dd($group->nom_groupe);
            $libellexit = Groupe::where('nom_groupe', $data['nom_groupe'])->where('user_id',Auth::user()->id)->first();

            if($group->nom_groupe == $data['nom_groupe'])
            {
                return redirect() -> route('gestiondesgroupes')->with('success','Groupe Modifié !');
            }else{
                if(!$libellexit)
                {
                    $group->nom_groupe = $data['nom_groupe'];
                    $group->save();
                    return redirect() -> route('gestiondesgroupes')->with('success','Groupe Modifié !');

                }else{
                    return redirect()->back()->with('error','Désolé le Groupe Existe Déjà !');
                }
            }
        }

        //return redirect() -> route('gestiondesgroupes')->with('error','Desoler Aucune Modification Effectuer !');
    }

    public function deletegroup($id)
    {
        $groups = DB::table('appartenir')->where('groupe_id',$id)->select('id')->get();
        //dd($groups);
        foreach($groups as $group):
            $delAppart = Appartenir::find($group->id);
            $delAppart->delete();
        endforeach;
        $groupDel = Groupe::find($id);
        $groupDel->delete();
        return redirect()->route('gestiondesgroupes')->with('success','Suppression effectuer ! ');
    }

    public function viewgroup($id, Request $request )
    {

        $verif = DB::table('groupe')->select('user_id')->where('id',$id)->first();

        if ($verif->user_id != Auth::user()->id) {
            return redirect()->route('gestiondesgroupes')->with('error','Désolé ! l\'identifiant du groupe saisir n\'est dans vos données!');

        }else{
            $groupename = Groupe::find($id);

            $group = $groupename->nom_groupe;
            //dd($group);
            $contacts = DB::table('appartenir')
                ->join('contact', 'appartenir.contact_id', '=', 'contact.id')
                ->join('groupe', 'appartenir.groupe_id', '=', 'groupe.id')
                ->where('appartenir.groupe_id',$id)
                ->select('contact.*')
                ->get();
            //dd($contacts);

            if(!empty($contacts))
            {
                return view('viewgroup',['id'=>$id])->with('contacts',$contacts)->with('group',$group);
            }

            return view('viewgroup',['id'=>$id])->with('group',$group);

        }
        
    }

    public function addContactgroup($id)
    {
        $verif = DB::table('groupe')->select('user_id')->where('id',$id)->first();
        //dd($verif->user_id);
        if ($verif->user_id != Auth::user()->id) {

            return redirect()->route('gestiondesgroupes')->with('error','Désolé ! l\'identifiant du groupe saisir n\'est dans vos donnée!');

        }else{
            $groupe = Groupe::find($id);
            $groupename = $groupe->nom_groupe;

            $data = DB::table('contact')
                    ->where('user_id', Auth::user()->id)
                    ->where('etat_sup','1')
                    ->orderBy('nom_contact','asc')
                    ->get();
            return view('addContactGroup',['id'=>$id])->with('data',$data)->with('groupename',$groupename);;
        }
        

    }

    public function deleteContactGroup(Request $request )
    {
        $q = $request->except(['_token']);
        $idContact = $q['idContact'];
        $idGroup = $q['idGroup'];
//        dd($idContact);
        $data = DB::table('appartenir')
                    ->select('id')
                    ->where('contact_id','=',$idContact)
                    ->where ('groupe_id','=',$idGroup)
                    ->first();

        $group = Appartenir::find($data->id);
        $group->delete();
        return redirect()->route('viewgroup',['id'=>$idGroup])->with('success','Suppression effectuer ! ');
        //dd($group);
        //dd($idContact);
        //return view('addContactGroup',['id'=>$id])->with('data',$data);
    }

    public function addAppartenir(Request $request)
    {


        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'IdContact' =>'required'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $dataContact = $reponse['IdContact'];
            foreach($dataContact as $data):

                $contact = new Appartenir();

                $contact->contact_id = $data;
                $contact->groupe_id = $reponse['IdGroup'];

                $seach = DB::table('appartenir')->where('contact_id','=',$contact->contact_id)
                    ->where ('groupe_id','=',$contact->groupe_id)
                    ->count();
                //dd()

                if($seach==0){
                    $contact->save();
                }
            endforeach;

            return redirect()->route('gestiondesgroupes')->with('success','Félicitation ! Nouveau Contact Ajouter aux groupe!');
        }
    }
}
?>
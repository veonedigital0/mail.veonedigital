<?php

namespace smsgestion\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use smsgestion\Contact;
use smsgestion\Groupe;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;
use smsgestion\Sms;
use smsgestion\User;
use Symfony\Component\HttpKernel\Client;
###################### infobip api ###################
use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;
#####################################################


class SmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //return url('/login');
    }

    public function gestiondessms()
    {
        $data = DB::table('groupe')
                ->where('user_id', Auth::user()->id)
                ->orderBy('nom_groupe','asc')
                ->get();

        $result = DB::table('contact')
                ->where('user_id', Auth::user()->id)
                ->where('etat_sup','1')
                ->orderBy('nom_contact','asc')
                ->get();
        return view('gestiondessms')->with('data',$data)->with('result',$result);
    }

    public function addsms()
    {
        $recharge = DB::table('log_rechargements')
            ->where('id_user',Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

        return view('addsms')->with('recharge',$recharge);
    }

    public function addSmsPost(Request $request)
    {
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'montantRecharge' =>'min:30|required',
        ]);

        //dd($reponse);
        if($valider->fails()){
            return redirect()->route('addsms')->withErrors($valider->errors());
        }else{
            // ENVOI DES INFORMATION SUR UNE AUTRE PAGE WEB
//            $date=new \DateTime(null);
//            $dateRecharge = $date->format("d-m-Y H-i-s");
//            $randAleatoire = mt_rand();


        }
    }


    public function validdessms(Request $request)
    {
        ##########################################
        //require_once __DIR__ . '/../../vendor/autoload.php';
        //include(app_path().'\..\vendor\autoload.php');
        require_once '/home/cx2jkqt3/sms.veonedigital.com/vendor/autoload.php';
        // Initializing SendSingleTextualSms client with appropriate configuration
        $client = new SendSingleTextualSms(new BasicAuthConfiguration('SIONVIEW', 'zAgg2016'));
        ##########################################

        $resultat = $request->except(['_token'])
        ;
        //COMPTER LE NOMBRE DE SMS
        $nbchar = mb_strlen($resultat['message']);
        $nbtexto = ceil($nbchar /150);

        // $msg =htmlspecialchars(trim($resultat['message']));
        // dd($msg);

################# API ENVOIE SMS ########################################################

//recuperation du nom de l'entreprise
        $id_compte = User::find($resultat['id_user']);
        $name_compte = $id_compte->nom_compte;

      //COMPTE NOMBRE TOTAL DENVOIE DE MESSAGE
        $initial= 0;
        if(!empty($resultat['groupe'])):
            $dataGroup = $resultat['groupe'];
            foreach($dataGroup as $group):
                $counts = DB::table('appartenir')->select('contact_id')->where('groupe_id', $group)->count();
                $nbSms = $counts + $initial;
            endforeach;
        endif;
        if(!empty($resultat['contact'])):
            $dataContact = $resultat['contact'];
            if(empty($resultat['groupe'])):
                $nbSms=0;
                foreach($dataContact as $contact):
                    $counts = DB::table('contact')->select('numero_contact')->where('id', $contact)->where('etat_sup','1')->count();
                    $nbSms = $counts + $nbSms ;
                endforeach;
            else:
                foreach($dataContact as $contact):
                    $counts = DB::table('contact')->select('numero_contact')->where('id', $contact)->where('etat_sup','1')->count();
                    $nbSms = $counts + $nbSms ;
                endforeach;
            endif;
        endif;

        //FIN DU COMPTAGE

        //dd($nbSms);
        
        //savoir si le nombre SMS est suffisant pour l'envoi des messages

        $nbSmspersn = $nbSms * $nbtexto;

        if($id_compte->nbresms_compte>= $nbSmspersn):

           
                
                //Epuration du nom de compte
                $SENDER_ADDRESS = str_replace(" ", "_", $name_compte);

                // Recuperation des contacts dans le groupe
                if (!empty($resultat['groupe'])):
                    $dataGroup = $resultat['groupe'];

                    foreach ($dataGroup as $group):
                        $qs = DB::table('appartenir')->select('contact_id')->where('groupe_id', $group)->get();
                        //dd($qs);
                        foreach ($qs as $q):
                            $Num = DB::table('contact')->select('numero_contact')->where('id', $q->contact_id)->where('etat_sup','1')->first();
                            $sms = new Sms();
                            $sms->destinataire_msg = $Num->numero_contact;
                            $sms->message = htmlspecialchars(trim($resultat['message']));
                            $sms->user_id = $resultat['id_user'];
                            $sms->status_msg = 1;


                            // Creating request body
                            $requestBody = new SMSTextualRequest();
                            $requestBody->setFrom($SENDER_ADDRESS);
                            $requestBody->setTo([$sms->destinataire_msg]);
                            $requestBody->setText(htmlspecialchars(trim($sms->message)));
                            
                            try {
                            //Envoi du sms
                            $response = $client->execute($requestBody);
                            }catch (Exception $exc) {
                            //insertion dans la table historique transaction
                            $sms->status_msg = 0;
                            }

                            //insertion dans la table historique transaction
                            $sms->save();

                            // Soustraction du nombre de sms restant
                            $smsRestant = ($id_compte->nbresms_compte) - $nbtexto;
                            $id_compte->nbresms_compte = $smsRestant;
                            //Update du nombre de sms
                            $id_compte->save();

                        endforeach;

                    endforeach;

                endif;
                //Fin recuperation de contacts dans le groupe

                if (!empty($resultat['contact'])):

                    $dataContact = $resultat['contact'];

                    //dd($NumCont->numero_contact);
                    foreach ($dataContact as $contact):
                        $NumCont = DB::table('contact')->select('numero_contact')->where('id', $contact)->where('etat_sup','1')->first();
                        $sms = new Sms();
                        $sms->destinataire_msg = $NumCont->numero_contact;
                        $sms->message = htmlspecialchars(trim($resultat['message']));
                        $sms->user_id = $resultat['id_user'];
                        $sms->status_msg = 1;

                       // Creating request body
                            $requestBody = new SMSTextualRequest();
                            $requestBody->setFrom($SENDER_ADDRESS);
                            $requestBody->setTo([$sms->destinataire_msg]);
                            $requestBody->setText(htmlspecialchars(trim($sms->message)));
                            
                            try {
                            //Envoi du sms
                            $response = $client->execute($requestBody);
                            }catch (Exception $exc) {
                            //insertion dans la table historique transaction
                            $sms->status_msg = 0;
                            }
                        //insertion dans la table
                        $sms->save();

                        // Soustraction du nombre de sms restant

                        $smsRestant = ($id_compte->nbresms_compte) - $nbtexto;
                        $id_compte->nbresms_compte = $smsRestant;
                        //Update du nombre de sms
                        $id_compte->save();

                    endforeach;
                endif;

                return redirect()->route('gestiondessms')->with('success', 'SMS envoye avec succes');

                
        else:
            return redirect()->route('gestiondessms')->with('error', 'Désolé le nombre de SMS est inférieure aux messages a envoyés');
        endif;

    }
######################################################### FIN API ENVOIE SMS ##################################################




    public function validdessmsParticulier(Request $request)
    {
        ##########################################
        //require_once __DIR__ . '/../../vendor/autoload.php';
        // include(app_path().'\..\vendor\autoload.php');
        require_once '/home/cx2jkqt3/sms.veonedigital.com/vendor/autoload.php';
        // Initializing SendSingleTextualSms client with appropriate configuration
        $client = new SendSingleTextualSms(new BasicAuthConfiguration('SIONVIEW', 'zAgg2016'));
        ##########################################

        $req = $request->except(['_token']);
        //dd($resultat);

        //COMPTER LE NOMBRE DE SMS
        $nbchar = mb_strlen($req['message']);
        $nbtexto = ceil($nbchar /150);

############################### API ENVOIE SMS POUR UN PARICULIER ################################################

//recuperation du nom de l'entreprise
        $id_compte = User::find($req['iduser']);
        $name_compte = $id_compte->nom_compte;

        //dd($id_compte->id);
        //COMPTE NOMBRE TOTAL DENVOIE DE MESSAGE
        $counts = DB::table('contact')
                    ->where('user_id',$id_compte->id)
                    ->where('etat_sup','1')
                    ->count();

        //FIN DU COMPTAGE

        //savoir si le nombre SMS est suffisant pour l'envoi des messages
        if($counts > 0 and $id_compte->nbresms_compte >= $counts):
            //dd($counts);
           
               
                
                //Epuration du nom de compte
                $SENDER_ADDRESS = str_replace(" ", "_", $name_compte);

                $dataContact = DB::table('contact')
                                    ->select('numero_contact')
                                    ->where('user_id',$req['iduser'])
                                    ->where('etat_sup','1')
                                    ->get();

                    //dd($NumCont->numero_contact);
                    foreach ($dataContact as $contact):
                        $sms = new Sms();
                        $sms->destinataire_msg = $contact->numero_contact;
                        $sms->message = htmlspecialchars(trim($req['message']));
                        $sms->user_id = $req['iduser'];
                        $sms->status_msg = 1;

                        // Creating request body
                            $requestBody = new SMSTextualRequest();
                            $requestBody->setFrom($SENDER_ADDRESS);
                            $requestBody->setTo([$sms->destinataire_msg]);
                            $requestBody->setText(htmlspecialchars(trim($sms->message)));
                            
                            try {
                            //Envoi du sms
                            $response = $client->execute($requestBody);
                            }catch (Exception $exc) {
                            //insertion dans la table historique transaction
                            $sms->status_msg = 0;
                            }

                        //insertion dans la table
                        $sms->save();

                        // Soustraction du nombre de sms restant
                        $smsRestant = ($id_compte->nbresms_compte) - $nbtexto;
                        $id_compte->nbresms_compte = $smsRestant;
                        //Update du nombre de sms
                        $id_compte->save();

                    endforeach;

                return redirect()->route('gestiondessms')->with('success', 'SMS envoye avec succes');

        else:
            return redirect()->route('gestiondessms')->with('error', 'Désolé le nombre de SMS est inférieure aux messages a envoyés');
        endif;

    }
############################ FIN  API ENVOIE SMS POUR UN PARICULIER ##################################################
}
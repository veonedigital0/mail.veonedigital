<?php

namespace smsgestion\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;

class HistoriqueController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //return url('/login');
    }


    public function historique()
    {
        return view('historique');
    }

    public function showHistorique(Request $request)
    {
        $resulte = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'Date_Debut' =>'required|date',
            'Date_Fin' =>'required|date|after:Date_Debut',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $dteD = explode('-', substr($resulte['Date_Debut'],0,10));
            $dteF = explode('-', substr($resulte['Date_Fin'],0,10));

            $dteDebut = $dteD[2].'-'.$dteD[1].'-'.$dteD[0];
            $dteFin = $dteF[2].'-'.$dteF[1].'-'.$dteF[0];

            $historique = DB::table('sms')
                ->where('user_id',$resulte['idUser'])
                ->whereBetween('created_at',[$dteDebut,$dteFin])
                ->orderBy('created_at', 'desc')
                ->get();

            if(isset($historique)&& !empty($historique))
            {
                return view('historique')->with('historique',$historique);

            }else{
                return redirect()->route('historique')->with('error','Désolé aucun resultat pour votre recherche!');
            }
        }
    }

}
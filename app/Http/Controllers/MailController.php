<?php

namespace smsgestion\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use smsgestion\Savemail;
use smsgestion\Sendmail;
use smsgestion\Template;

class MailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function campagne(){
        /*if (Auth::user()->mailing!=1){
            return redirect()->route('gestiondessms')->with('error', 'Désolé vous n\'avez pas droit à la page!');
        }*/
        $temp = Template::where('user_id',null)->get();
        $tempUser = Template::where('user_id',Auth::user()->id)->get();
        return view("campagnemail",compact('temp','tempUser'));
    }

    public function template(){
        $temps = Template::where('user_id',Auth::user()->id)->get();
        //dd($temps);
        return view("templatemail",compact('temps'));
    }

    public function create(){
        return view("createtemplate");
    }

    public function savetemplate(Request $request){
        $reponse = $request->except(['_token']);
        $nmefile= $request->file('fileTemple');

        $valider = Validator::make($request->all(),[
            'nametemple' =>'max:255|min:3|required',
            'contenu' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('create-template')->withErrors($valider->errors());
        }else{

            $fileName=$nmefile->getClientOriginalName();
            $fileextensions=$nmefile->getClientOriginalExtension();
            $fileSize=$nmefile->getClientSize();

            try{
                if($fileSize <= 3000263):
                    $aleatoire=str_random(8);
                    $uploadName = htmlspecialchars(trim(strtolower($fileName)));
                    $upload_nom =$aleatoire.$uploadName;
                    $upload_dest= 'templates/';
                    $fileextension=strtolower($fileextensions);
                    $extensions_autorisees=array('jpeg','jpg','gif','tiff','bmp','png');

                    if(in_array($fileextension ,$extensions_autorisees)):

                        $template = new Template();
                        $template->nom = $reponse['nametemple'];
                        $template->img = $upload_dest.$upload_nom;
                        $template->scr = $reponse['contenu'];
                        $template->user_id = Auth::user()->id;
                        $template->save();

                        $nmefile->move($upload_dest,$upload_nom);

                        return redirect()->route('mytemplates')->with('success','Félicitation ! votre template a été ajouté');
                    else:
                        return redirect()->route('create-template')->with('error','Désolé ! L\'extension de l\'image n\'est pas autorise');
                    endif;
                else:
                    return redirect()->route('create-template')->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
                endif;

            }catch (\Exception $e){
                dd($e->getMessage());
            }
            //dd($reponse);
        }
    }



    public function index($id){
        //dd($id);
        if (!isset($id)){
            return redirect()->route('campagne');
        }
        $data = DB::table('groupe')->where('user_id', Auth::user()->id)
            ->orderBy('nom_groupe','asc')
            ->get();

        $result = DB::table('contact')->where('user_id', Auth::user()->id)->where('etat_sup','1')
            ->orderBy('nom_contact','asc')
            ->get();

        $scr = DB::table('templates')->select('scr')
            ->where('id',$id)
            ->first();

        if ($scr==null){
            $Vscr="";
        }else{
            $Vscr=$scr->scr;
        }
        //dd($Vscr);
        return view("mail")->with('idtemp',$id)->with('data',$data)->with('result',$result)->with('source',$Vscr);
    }

    public function edit($id){
        //dd($id);
        if (!isset($id)){ return redirect()->route('campagne'); }
        $data = DB::table('templates')->where('id',$id)->where('user_id',Auth::user()->id)->first();
        //dd($data);
        return view("editmail")->with('data',$data);
    }

    public function editsavemail(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);

        $valider = Validator::make($request->all(),[
            'nametemple' =>'max:255|min:3|required',
            'contenu' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('create-template')->withErrors($valider->errors());
        }else{

            if($request->hasFile('fileTemple')){
                $nmefile = $request->file('fileTemple');

                $fileName=$nmefile->getClientOriginalName();
                $fileextensions=$nmefile->getClientOriginalExtension();
                $fileSize=$nmefile->getClientSize();

                    if($fileSize <= 3000263):
                        $aleatoire=str_random(8);
                        $uploadName = htmlspecialchars(trim(strtolower($fileName)));
                        $upload_nom =$aleatoire.$uploadName;
                        $upload_dest= 'templates/';
                        $fileextension=strtolower($fileextensions);
                        $extensions_autorisees=array('jpeg','jpg','gif','tiff','bmp','png');

                        if(in_array($fileextension ,$extensions_autorisees)):

                            $template = Template::where('id',$reponse['idtemp'])->firstOrFail();

                            if (!empty($template->img)) {
                                unlink($template->img);
                            }

                            $template->nom = $reponse['nametemple'];
                            $template->img = $upload_dest.$upload_nom;
                            $template->scr = $reponse['contenu'];
                            $template->save();

                            $nmefile->move($upload_dest,$upload_nom);

                            return redirect()->route('mytemplates')->with('success','Félicitation ! votre template a été ajouté');
                        else:
                            return redirect()->route('create-template')->with('error','Désolé ! L\'extension de l\'image n\'est pas autorise');
                        endif;
                    else:
                        return redirect()->route('create-template')->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
                    endif;

            }else{
                $template = Template::where('id',$reponse['idtemp'])->firstOrFail();
                $template->nom = $reponse['nametemple'];
                $template->scr = $reponse['contenu'];
                $template->save();

                return redirect()->route('mytemplates')->with('success','Félicitation ! votre template a été ajouté');
            }
            //dd($reponse);
        }
    }

    public function sendmail(Request $request){
        $reponse = $request->except(['_token']);

        $campagname = $reponse['campagname'];
        $idtemp = $reponse['idtemp'];
        $sujet = $reponse['sujet'];
        $textBody = $reponse['contenu'];

        if(!empty($reponse['groupe'])):
            $dataGroupe = $reponse['groupe'];
            foreach($dataGroupe as $group):
                $qs = DB::table('appartenir')->select('contact_id')->where('groupe_id', $group)->get();
                if ($qs==null):
                    return redirect()->route('mail',['id'=>$idtemp])->with('error', 'Le groupe sélectionné ne contient aucun contact !');
                endif;
                foreach ($qs as $q):
                    $emailGroup = DB::table('contact')->select('email_contact')->where('id', $q->contact_id)->where('etat_sup','1')->first();
                    $arrayEmail[]=$emailGroup->email_contact;
                endforeach;
            endforeach;
        endif;

        if(!empty($reponse['contact'])){
            $dataContact = $reponse['contact'];
            if(!empty($reponse['groupe'])){
                $allEmail = array_merge($arrayEmail, $dataContact);
            }else{
                $allEmail = $dataContact;
            }
        }else{
            if(!empty($reponse['groupe'])){
                $allEmail = $arrayEmail;
            }else{
                $allEmail = "";
            }
        }

        $comma_separated = implode(",", $allEmail);
        //dd($comma_separated);

        if (empty($allEmail)){
            return redirect()->route('mail',['id'=>$idtemp])->with('error', 'Veuillez choisir les destinataires !');
        }

        $savecampagne = new Savemail();
        $savecampagne->nomcampagne = $campagname;
        $savecampagne->sujet = $sujet;
        $savecampagne->user_id = Auth::user()->id;
        $savecampagne->listemail = $comma_separated;
        $savecampagne->mail = $textBody;
        $savecampagne->save();

        foreach($allEmail as $itemMail){
            $sendmail = new Sendmail();
            $sendmail->email = $itemMail;
            $sendmail->sujet = $sujet;
            $sendmail->src = $textBody;
            $sendmail->emailuser = Auth::user()->email;
            $sendmail->save();
        }



        return redirect()->route('campagne')->with('success', 'Votre mail a été envoyer avec succes !');


        /*try{
            $data=array(
                "textBody"=>$textBody,
                "sujets"=>$sujet
            );

            foreach($allEmail as $itemMail){
                Mail::send('mail/email',$data,function($message) use($sujet,$itemMail){
                    $message->to($itemMail);
                    $message->from(Auth::user()->email);
                    $message->subject($sujet);
                });
            }

            if(count(Mail::failures())>0){
                return redirect()->route('campagne')->with('error', 'Erreur lors de l\'envoie du mail !');
            }else{
                return redirect()->route('campagne')->with('success', 'Votre mail a été envoyer avec succes !');
            }

        } catch(\Exception $e){
            //dd($e->getMessage());
            return redirect()->route('campagne')->with('error', 'Erreur lors de l\'envoie du mail !');
        }*/


    }

    /*public function sendmail(Request $request){
        $reponse = $request->except(['_token']);
        dd($reponse);

        $campagname = $reponse['campagname'];
        $idtemp = $reponse['idtemp'];
        $sujet = $reponse['sujet'];
        $textBody = $reponse['contenu'];

        if(!empty($reponse['groupe'])):
            $dataGroupe = $reponse['groupe'];
            foreach($dataGroupe as $group):
                $qs = DB::table('appartenir')->select('contact_id')->where('groupe_id', $group)->get();
                if ($qs==null):
                    return redirect()->route('mail',['id'=>$idtemp])->with('error', 'Le groupe sélectionné ne contient aucun contact !');
                endif;
                foreach ($qs as $q):
                    $emailGroup = DB::table('contact')->select('email_contact')->where('id', $q->contact_id)->where('etat_sup','1')->first();
                    $arrayEmail[]=$emailGroup->email_contact;
                endforeach;
            endforeach;
        endif;

        if(!empty($reponse['contact'])){
            $dataContact = $reponse['contact'];
            if(!empty($reponse['groupe'])){
                $allEmail = array_merge($arrayEmail, $dataContact);
            }else{
                $allEmail = $dataContact;
            }
        }else{
            if(!empty($reponse['groupe'])){
                $allEmail = $arrayEmail;
            }else{
                $allEmail = "";
            }
        }

        if (empty($allEmail)){
            return redirect()->route('mail',['id'=>$idtemp])->with('error', 'Veuillez choisir les destinataires !');
        }

        try{
            $data=array(
                "textBody"=>$textBody,
                "sujets"=>$sujet
            );

            foreach($allEmail as $itemMail){
                Mail::send('mail/email',$data,function($message) use($sujet,$itemMail){
                    $message->to($itemMail);
                    $message->from(Auth::user()->email);
                    $message->subject($sujet);
                });
            }

            if(count(Mail::failures())>0){
                return redirect()->route('campagne')->with('error', 'Erreur lors de l\'envoie du mail !');
            }else{
                return redirect()->route('campagne')->with('success', 'Votre mail a été envoyer avec succes !');
            }

        } catch(\Exception $e){
            //dd($e->getMessage());
            return redirect()->route('campagne')->with('error', 'Erreur lors de l\'envoie du mail !');
        }


    }*/

}

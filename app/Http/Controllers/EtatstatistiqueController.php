<?php

namespace smsgestion\Http\Controllers;

use smsgestion\Contact;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;

class EtatstatistiqueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //return url('/login');
    }

    public function etatstatistique()
    {
        return view('etatstatistique');
    }


}

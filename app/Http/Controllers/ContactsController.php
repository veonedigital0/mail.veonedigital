<?php

namespace smsgestion\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use smsgestion\Appartenir;
use smsgestion\Contact;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        return url('/login');
    }

    public function gestioncontacts()
    {
        $contacts = DB::table('contact')
                        ->where('user_id', Auth::user()->id)
                        ->where('etat_sup','1')
                        ->orderBy('nom_contact','asc')->get();
        return view('gestiondescontacts')->with('contacts', $contacts);
    }

//recuperation des champs du formulaire

    public function validdescontacts(Request $request)
    {
        $reponse = $request->except(['_token']);
        // $contact=$reponse['indic_contact'].$reponse['numero_contact'];
        // dd($contact);
        $valider = Validator::make($request->all(),[
            'nom_contact' =>'max:255|min:3|required',
            'prenoms_contact' =>'max:255|min:3|required',
            'email_contact' =>'email|required',
            //'fonction_contact' =>'max:255|min:3|required',
            //'numero_contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('gestioncontacts')->withErrors($valider->errors());
        }else{
            $numero_exit = Contact::where('email_contact',$reponse['email_contact'])->where('user_id',Auth::user()->id)->where('etat_sup','1')->first();

            if(!$numero_exit){

                    $contact = new Contact();

                    $contact->nom_contact = $reponse['nom_contact'];
                    $contact->prenoms_contact = $reponse['prenoms_contact'];
                    $contact->email_contact = $reponse['email_contact'];
                    $contact->numero_contact = $reponse['numero_contact'];
                    $contact->fonction_contact = $reponse['fonction_contact'];
                    $contact->user_id = $reponse['id_user'];

                    $contact->save();

                    return redirect()->route('gestioncontacts')->with('success', 'Contact ajoute avec succes !');

            } else{
                return redirect()->back()->with('error','Désolé le Contact Existe Déjà !');
            }

        }



    }

    public function validdescontactsparticulier(Request $request)
    {
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom_contact' =>'max:255|min:3|required',
            'prenoms_contact' =>'max:255|min:3|required',
            'email_contact' =>'email|required',
            //'fonction_contact' =>'max:255|min:3|required',
            'numero_contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('gestioncontacts')->withErrors($valider->errors());
        }else{
            $numero_exit = Contact::where('numero_contact',$reponse['numero_contact'])->where('user_id',Auth::user()->id)->where('etat_sup','1')->first();

            if(!$numero_exit){

                $countContact = DB::table('users')
                ->join('contact', 'users.id', '=', 'contact.user_id')
                ->where('users.type_compte','particulier')
                ->where('contact.etat_sup','1')
                ->select('contact.id')
                ->count();

                //dd($countContact);

                if ($countContact>=20) {
                    return redirect()->back()->with('error','Désolé vous avez droit à 20 Contacts !');
                }else{
                    $contact = new Contact();

                    $contact->nom_contact = $reponse['nom_contact'];
                    $contact->prenoms_contact = $reponse['prenoms_contact'];
                    $contact->email_contact = $reponse['email_contact'];
                    $contact->numero_contact = $reponse['indic_contact'].$reponse['numero_contact'];
                    $contact->fonction_contact = $reponse['fonction_contact'];
                    $contact->user_id = $reponse['id_user'];

                    $contact->save();

                    return redirect()->route('gestioncontacts')->with('success', 'Contact ajoute avec succes !');
                }
                
            } else{
                return redirect()->back()->with('error','Désolé le Contact Existe Déjà !');
            }

        }



    }


    public function modifcontacts($id)
    {
        $verif = DB::table('contact')->select('user_id')->where('id',$id)->first();
        //dd($verif->user_id);
        if ($verif->user_id != Auth::user()->id) {

            return redirect()->route('gestioncontacts')->with('error','Désolé ! l\'identifiant du contact saisir n\'est dans vos donnée!');

        }else{
            $modif = Contact::find($id);
            return view('updatecontacts')->with('modif', $modif);
        }


    }


    public function updatecontacts(Request $request, $id)
    {
        $update = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom_contact' =>'max:255|min:3|required',
            'prenoms_contact' =>'max:255|min:3|required',
            'email_contact' =>'email|required',
            //'fonction_contact' =>'max:255|min:3|required',
            //'numero_contact' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('modifcontacts',['$id' => $id])->withErrors($valider->errors());
        }else{
            $numero = Contact::where('id',$id)->where('user_id',Auth::user()->id)->where('etat_sup','1')->first();
            //dd($numero);
            if($numero->numero_contact == $update['numero_contact'] )
            {
                $contact = Contact::find($id);
                if($request-> isMethod('post'))
                {
                    $contact->nom_contact = $update['nom_contact'];
                    $contact->prenoms_contact = $update['prenoms_contact'];
                    $contact->email_contact = $update['email_contact'];
                    $contact->fonction_contact = $update['fonction_contact'];
                    $contact->user_id = $update['id_user'];

                    $contact->save();
                    return redirect()->route('gestioncontacts')->with('success', 'Contact mis a jour avec succes !');
                }
            }else{
                $numero_exit = Contact::where('email_contact',$update['email_contact'])->where('user_id',Auth::user()->id)->where('etat_sup','1')->first();
                if(!$numero_exit)
                {
                    $contact = Contact::find($id);
                    if($request-> isMethod('post'))
                    {
                        $contact->nom_contact = $update['nom_contact'];
                        $contact->prenoms_contact = $update['prenoms_contact'];
                        $contact->email_contact = $update['email_contact'];
                        $contact->fonction_contact = $update['fonction_contact'];
                        $contact->numero_contact = $update['numero_contact'];
                        $contact->user_id = $update['id_user'];

                        $contact->save();
                        return redirect()->route('gestioncontacts')->with('success', 'Contact mis à jour avec succès !');
                    }
                }else{
                    return redirect()->back()->with('error','Désolé Modification impossible car le contact existe déjà !');
                }
            }

        }
        //return view('gestiondescontacts')->with('contact', $contact);
    }

    public function deletecontacts($id)
    {
        //$contact= Contact::find($id);
        $requete = DB::table('appartenir')->where('contact_id',$id)->get();
        //dd($requete->id);
        foreach($requete as $q):
            $delAppart = Appartenir::find($q->id);
            $delAppart->delete();
        endforeach;

        $contactDel = Contact::find($id);
        $contactDel->etat_sup = '0';
        $contactDel->save();

        return redirect()-> route('gestioncontacts')->with('success', 'Contact supprimé avec succès');
    }

    public function addExcelContact(Request $request )
    {
        $excel = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'file' =>'required|mimes:xls,xlsx',
        ]);

        if($valider->fails()){
            return redirect()->route('gestioncontacts')->withErrors($valider->errors());
        }else{
            $results = Excel::selectSheetsByIndex(0)->load($excel['file'])->toArray();
            //dd($results);
            if(!empty($results)){
                foreach($results as $value=>$key ){
                    $newContact = new Contact();

                    $newContact->nom_contact = $key["nom_contact"];
                    $newContact->prenoms_contact = $key["prenoms_contact"];
                    $newContact->email_contact = $key["email_contact"];
                    $newContact->numero_contact = $key["numero_contact"];
                    $newContact->fonction_contact = $key["fonction_contact"];
                    $newContact->user_id = Auth::user()->id;

                    $newContact->save();
                    //var_dump($key["prenoms_contact"]);
                }
                return redirect()-> route('gestioncontacts')->with('success', 'Contact Ajouté avec Succès');
            }
        }
    }


}

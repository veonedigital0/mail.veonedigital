<?php

namespace smsgestion\Http\Controllers;



use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use smsgestion\Http\Requests;
use Illuminate\Http\Request;
use smsgestion\User;

class ProfilController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        return url('/login');
    }

    public function profil()
    {
        return view('profil');
    }

    public function editeUserProfil(Request $request)
    {
        $requete= $request -> except(['_token']);

        $valider = Validator::make($request->all(),[
            'nom' =>'max:255|min:6|required',
            'prenom' =>'max:255|min:6|required',
            'adressCompte' =>'required|min:5|max:255',
            'sexe' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('profil')->withErrors($valider->errors());
        }else{
            Auth::user()->update([
                'name' => $requete['nom'],
                'prenoms' => $requete['prenom'],
                'sexe' => $requete['sexe'],
                'adresse_compte' => $requete['adressCompte'],
                'contact' => $requete['ContactCompte'],
            ]);

            return redirect()->route('profil')->with('success','Votre profil a été mise a jour');
            //dd($requete);
        }

    }

    public function editeUserPass(Request $request)
    {
        $requete= $request -> except(['_token']);

        $valider = Validator::make($request->all(),[
            'ancienpassword' =>'max:255|min:6|required',
            'newpassword' =>'max:255|min:6|required',
        ]);
//dd($valider->fails());
        if($valider->fails()){
            return redirect()->route('profil')->withErrors($valider->errors());
        }else{
            $oldpass = $requete['ancienpassword'];
            $newpass = $requete['newpassword'];
            $newpassconfirm = $requete['confirnewpassword'];

            $passUser = Auth::user()->password;

            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->pass_mobile = md5($newpass);
                    $user->secure = 1;
                    $user->save();

                    return redirect()->route('profil')->with('success','Votre mot de passe a été mise a jour');
                }else{
                    return redirect()->route('profil')->with('error','Désolé votre mot de passe actuel est erronée');
                }

            }else{
                return redirect()->route('profil')->with('error','Les mots de passe sont différents');
            }
        }

    }

}
?>
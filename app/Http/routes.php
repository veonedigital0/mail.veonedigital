<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

//definition des routes pour les pages avec formulaires

//PAGE DE PROFIL

Route::get('/profil', [
    'as' => 'profil',
    'uses' => 'ProfilController@profil',
]);

Route::post('/editeUserProfil', [
    'as' => 'editeUserProfil',
    'uses' => 'ProfilController@editeUserProfil',
]);

Route::post('/editeUserPass', [
    'as' => 'editeUserPass',
    'uses' => 'ProfilController@editeUserPass',
]);


//PAGE DE CONTACTS

Route::get('/gestioncontact', [
    'as' => 'gestioncontacts',
    'uses' => 'ContactsController@gestioncontacts',
]);

Route::get('/modifcontacts/{id}',[
    'as' => 'modifcontacts',
    'uses' => 'ContactsController@modifcontacts'
]);

Route::match(['get','post'],'/updatecontact/{id}',[
    'as' => 'updatecontacts',
    'uses' => 'ContactsController@updatecontacts'
]);

Route::get('/deletecontacts/{id}',[
    'as' => 'deletecontacts',
    'uses' => 'ContactsController@deletecontacts'
]);

Route::post('/validcontact', [
    'as' => 'validdescontacts',
    'uses' => 'ContactsController@validdescontacts',
]);

Route::post('/validcontactparticulier', [
    'as' => 'validdescontactsparticulier',
    'uses' => 'ContactsController@validdescontactsparticulier',
]);

Route::post('/excelcontact', [
    'as' => 'addExcelContact',
    'uses' => 'ContactsController@addExcelContact',
]);

//PAGE DE GROUPE
Route::get('/gestiongroupe', [
    'as' => 'gestiondesgroupes',
    'uses' => 'GroupeController@gestiondesgroupes',
]);

Route::post('/updategroupes/{id}', [
    'as' => 'updategroupes',
    'uses' => 'GroupeController@updategroupes',
]);

Route::get('/editgroup/{id}',[
    'as'=>'editgroup',
    'uses'=>'GroupeController@editgroup',
]);

Route::get('/viewgroup/{id}',[
    'as'=>'viewgroup',
    'uses'=>'GroupeController@viewgroup',
]);

Route::get('/addContactgroup/{id}',[
    'as'=>'addContactgroup',
    'uses'=>'GroupeController@addContactgroup',
]);

Route::post('/addAppartenir',[
    'as'=>'addAppartenir',
    'uses'=>'GroupeController@addAppartenir',
]);

Route::get('/deletegroup/{id}',[
    'as'=>'deletegroup',
    'uses'=>'GroupeController@deletegroup',
]);

Route::post('/deleteContactGroup', [
    'as' => 'deleteContactGroup',
    'uses' => 'GroupeController@deleteContactGroup',
]);

Route::post('/validgroupes', [
    'as' => 'validgroupes',
    'uses' => 'GroupeController@validgroupes',
]);



//PAGE DE SMS
Route::get('/gestionsms', [
    'as' => 'gestiondessms',
    'uses' => 'SmsController@gestiondessms',
]);

Route::post('/validsms', [
    'as' => 'validdessms',
    'uses' => 'SmsController@validdessms',
]);

Route::post('/validdessmsParticulier', [
    'as' => 'validdessmsParticulier',
    'uses' => 'SmsController@validdessmsParticulier',
]);

Route::get('/addsms', [
    'as' => 'addsms',
    'uses' => 'SmsController@addsms',
]);

Route::post('/addsmspost', [
    'as' => 'addsmspost',
    'uses' => 'SmsController@addsmsPost',
]);


//PAGE DE RECHARGEMENT
Route::get('/rechargement', [
    'as' => 'logrechargement',
    'uses' => 'RechargementController@logrechargement',
]);

Route::match(['get','post'],'/showLogRechar', [
    'as' => 'showLogRechar',
    'uses' => 'RechargementController@showLogRechar',
]);


//PAGE DE CONNEXION
Route::get('/connection', [
    'as' => 'logconnection',
    'uses' => 'ConnectionController@logconnection',
]);

//PAGE DE HISTORIQUE
Route::get('/historique', [
    'as' => 'historique',
    'uses' => 'HistoriqueController@historique',
]);

Route::match(['get','post'],'/showHistorique', [
    'as' => 'showHistorique',
    'uses' => 'HistoriqueController@showHistorique',
]);

//PAGE DE STATISTIQUE
Route::get('/statistique', [
    'as' => 'etatstatistique',
    'uses' => 'EtatstatistiqueController@etatstatistique',
]);

//PAGE DE MAIL

Route::get('/campagne', [
    'as' => 'campagne',
    'uses' => 'MailController@campagne',
]);

Route::get('/mail/{id}', [
    'as' => 'mail',
    'uses' => 'MailController@index',
]);

Route::get('/editmail/{id}', [
    'as' => 'editmail',
    'uses' => 'MailController@edit',
]);

Route::post('/editsavetemplate', [
    'as' => 'editsavetemplate',
    'uses' => 'MailController@editsavemail',
]);

Route::post('/sendmail', [
    'as' => 'sendmail',
    'uses' => 'MailController@sendmail',
]);

//PAGE DE MAIL ESPACE
Route::get('/mytemplates', [
    'as' => 'mytemplates',
    'uses' => 'MailController@template',
]);

Route::get('/create-template', [
    'as' => 'create-template',
    'uses' => 'MailController@create',
]);

Route::post('/savetemplate', [
    'as' => 'savetemplate',
    'uses' => 'MailController@savetemplate',
]);


<?php

namespace smsgestion;

use Illuminate\Database\Eloquent\Model;

class Log_rechargement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'log_rechargement';

    protected $fillable = [
        'nom_compte','nombre_sms',
    ];

}

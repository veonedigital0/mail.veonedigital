<?php

namespace globalsms;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_client','adresse_client','email_client', 'user_client', 'password_client','etat_client','sms_client',
        'smsenvoyer_client','smsrestant_client',

    ];

}

<?php 

	if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
        header('Access-Control-Allow-Origin: *');


	$namescript =  $_REQUEST['var'];
	

	// CONNEXION A LA BASE DE DONNEES 
	try{
	    $pdo_options [PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
	   	//$db = new PDO('mysql:host=localhost;dbname=smsgestion','root','',$pdo_options);
		$db = new PDO('mysql:host=localhost;dbname=pb1y14p0_sms','pb1y14p0_smsuser','pb1y14p0_smspwd',$pdo_options);

		}catch (PDOException $e){
	    	die('Base de donnees introuvable '.$e->getMessage());
		}


	// SE CONNECTER
		if($namescript=="connexion" ){

			$email=$_REQUEST['email'];
			$password=$_REQUEST['password'];

		    	$req = $db->prepare("SELECT * FROM users WHERE email = :email AND pass_mobile = :pass_mobile AND etat_compte=1");
		    	$req->execute(array(
				    'email'=>$email,
				    'pass_mobile'=>md5($password)
		         ));
			    
			    $num=$req->rowCount();

		    	if($num==1){

		    		$selecte = $req->fetch();
		    		$data[]=array (
						"id" => $selecte['id'],
						"nom_compte"  => $selecte['nom_compte'],
						"nbresms_compte"  => $selecte['nbresms_compte'],					
						"type_compte"  => $selecte['type_compte'],					
					);
					echo json_encode($data);

		    	}else{

		    		$data=0;
					echo json_encode($data);
		    	}		
		}


	    // AFFICHER TOUS LES CONTACTS
		if($namescript=="lists_contact"){

			$id_user =  $_REQUEST['id_user'];

		    	$req = $db->prepare("SELECT * FROM contact WHERE user_id = :id_user ORDER BY nom_contact ASC");
		    	$req->execute(array(
				    'id_user'=>$id_user
		         ));
				   
		        while($selecte = $req->fetch()){
		        	//echo "1";
					$data[]=array (
						"id" => $selecte['id'],
						"nomcontact"  => $selecte['nom_contact'],
						"prenomcontact"  => $selecte['prenoms_contact'],					
						"numerocontact"  => $selecte['numero_contact']					
					);

		        }

				echo json_encode($data);
			
		}


		// AFFICHER TOUS LES GROUPES
		if($namescript=="lists_groupe"){

			$id_user =  $_REQUEST['id_user'];

		    	$req = $db->prepare("SELECT * FROM groupe WHERE user_id = :id_user ORDER BY nom_groupe ASC");
		    	$req->execute(array(
				    'id_user'=>$id_user
		         ));
				   
		        while($selecte = $req->fetch()){
		        	//echo "1";
					$data[]=array (
						"id" => $selecte['id'],
						"nom_groupe"  => $selecte['nom_groupe'],
						"created_at"  => date("d/m/Y",strtotime($selecte['created_at']))					
					);

		        }

				echo json_encode($data);
			
		}


		// AFFICHER TOUS LES GROUPES
		if($namescript=="select_lists_groupe"){

			//$id_user =  $_REQUEST['id_user'];
			$id_group =  $_REQUEST['id_group'];

		    	$req = $db->prepare("SELECT * FROM appartenir,contact,groupe 
									  WHERE appartenir.contact_id=contact.id
									  AND   appartenir.groupe_id=groupe.id
									  AND   appartenir.groupe_id = :groupe_id");
		    	$req->execute(array(
				    'groupe_id'=>$id_group
		         ));
				   
		        while($selecte = $req->fetch()){
		        	//echo "1";
					$data[]=array (
						"id" => $selecte['id'],
						"nom_groupe"  => $selecte['nom_groupe'],
						"nomcontact"  => $selecte['nom_contact'],
						"prenomcontact"  => $selecte['prenoms_contact'],					
						"fonctioncontact"  => $selecte['fonction_contact'],					
						"numerocontact"  => $selecte['numero_contact']					
					);

		        }

				echo json_encode($data);
			
		}
		

   // AFFICHER LES HISTORIQUE EN FCTION DES DATES
		if($namescript=="historique"){

			$id_user =  $_REQUEST['id_user'];
			$dteDebut =  $_REQUEST['dtedebut'];
			$dteFin =  $_REQUEST['dtefin'];

			$dteDebutNew=date('Y-m-d', strtotime(substr($dteDebut,0,15)));
			$dteFinNew=date('Y-m-d', strtotime(substr($dteFin,0,15)));


			$req = $db->prepare("SELECT * FROM sms WHERE user_id = $id_user AND SUBSTR(created_at, 1,10) BETWEEN '$dteDebutNew' AND '$dteFinNew' ORDER BY created_at DESC");
		    	$req->execute();

			$num=$req->rowCount();

			if ($num>0) {
				while ($selecte = $req->fetch()) {
					//var_dump($selecte);
					$data[] = array(
						"destinataire_msg" => $selecte['destinataire_msg'],
						"message" => utf8_encode($selecte['message']),
						"created_at" => date("d/m/Y", strtotime($selecte['created_at']))
					);

				}
				echo json_encode($data);
			}
			else{
				$date=0;
				echo json_encode($data);
			}
		}
		


		// AFFICHER LES LOG RECHARGEMENT EN FCTION DES DATES
if($namescript=="rechargement"){

	$id_user =  $_REQUEST['id_user'];
	$dteDebut =  $_REQUEST['dtedebut'];
	$dteFin =  $_REQUEST['dtefin'];

	$dteDebutNew=date('Y-m-d', strtotime(substr($dteDebut,0,15)));
	$dteFinNew=date('Y-m-d', strtotime(substr($dteFin,0,15)));


	$req = $db->prepare("SELECT * FROM log_rechargements WHERE user_id = $id_user AND SUBSTR(created_at, 1,10) BETWEEN '$dteDebutNew' AND '$dteFinNew'  ORDER BY created_at DESC ");
	$req->execute();

	$num=$req->rowCount();

	if ($num>0) {
		while ($selecte = $req->fetch()) {
			//var_dump($selecte);
			$data[]=array (
				"nom_recharge"  => $selecte['nom_recharge'],
				"libelle_recharge"  => $selecte['libelle_recharge'],
				"totalsms_recharge"  => $selecte['totalsms_recharge'],
				"created_at"  => date("d/m/Y",strtotime($selecte['created_at']))
			);

		}
		echo json_encode($data);
	}
	else{
		$date=0;
		echo json_encode($data);
	}
}




		// SELECTIONNER TOUS LES GROUPE DE LUSERS
		if($namescript=="selectGroup"){

			$id_user =  $_REQUEST['id_user'];

		    	$req = $db->prepare("SELECT * FROM groupe WHERE user_id = :user_id ORDER BY nom_groupe ASC ");
		    	$req->execute(array('user_id'=>$id_user));				
				
		        while($selecte = $req->fetch()){

					$data[]=array (
						"id_groupe"  => $selecte['id'],
						"nom_groupe"  => $selecte['nom_groupe']					
					);

		        } 

				if (empty($data)) {
		        	$data=0;
		        	echo json_encode($data);
		        }else{
		        	 echo json_encode($data);
		        }
			
		}


		// SELECTIONNER TOUS LES CONTACTS DE LUSERS
		if($namescript=="selectCont"){

			$id_user =  $_REQUEST['id_user'];

		    	$req = $db->prepare("SELECT * FROM contact WHERE user_id = :user_id AND etat_sup='1' ORDER BY nom_contact ASC ");
		    	$req->execute(array('user_id'=>$id_user));				
				
		        while($selecte = $req->fetch()){
		        		        	
					$data[]=array (
						"id_contact"  => $selecte['id'],
						"nom_contact"  => $selecte['nom_contact'],					
						"prenom_contact"  => $selecte['prenoms_contact'],					
					);

		        } 

				if (empty($data)) {
		        	$data=0;
		        	echo json_encode($data);
		        }else{
		        	 echo json_encode($data);
		        }
			
		}


		// ENVOYER LE SMS
		if($namescript=="ValidSms"){
			// var_dump($message);
			//header('Access-Control-Allow-Origin: *');
			
			$id_user =  $_REQUEST['id_user'];
			$groupe =  $_REQUEST['groupe'];
			$contact =  $_REQUEST['contact'];
			$message =  $_REQUEST['message'];
			$nbsms =  $_REQUEST['nb_sms'];
			$name_compte =  $_REQUEST['nomCompte'];
		    
			$nbchar = mb_strlen($message);
			$nbtexto = ceil($nbchar / 150);

			$Selectgroupe = explode(',', $groupe);
			$Selectcontact = explode(',', $contact);

				
			//COMPTAGE
			$initial= 0;
			if($groupe!='undefined'){				
				 $nbr= 0;
				foreach ($Selectgroupe as $key => $value) {
					$qes = $db->query("SELECT * FROM appartenir WHERE groupe_id = $value");
					$count = $qes->rowCount(); 
					$nbr = $nbr + $count;
				}
			}   
		    

		    if($contact!='undefined'){
		        if($groupe=='undefined'){
		            $nbr=0;
		            foreach($Selectcontact as $key => $value){
		                $qes =$db->query("SELECT * FROM contact WHERE id = $value AND etat_sup='1'");
		            	$count = $qes->rowCount();
		                $nbr = $count + $nbr ;
		            }

		        }else{
		            foreach($Selectcontact as $key => $value){
		                $qes =$db->query("SELECT * FROM contact WHERE id = $value AND etat_sup='1'");
		            	$count = $qes->rowCount();		            	
		                $nbr= $nbr + $count ;
		            }

		        }
		    }

		    if ($groupe=='undefined' AND $groupe=='undefined') {
		    	$nbr=0;
		    }
		    
		    //FIN DU COMPTAGE
		    require_once '../ Symfony/Component/HttpKernel/Client';
			###################### infobip api ###################
			require_once '../ infobip/api/client/SendSingleTextualSms';
			require_once '../ infobip/api\configuration/BasicAuthConfiguration';
			require_once '../ infobip/api/model/sms/mt/send/textual/SMSTextualRequest';
			#####################################################
			
		    require_once '/home/pb1y14p0/sms.sion-view.net/vendor/autoload.php';
		    //require_once '../vendor/autoload.php';
		    var_dump($message);//	die();
			$client = new SendSingleTextualSms(new BasicAuthConfiguration('SIONVIEW', 'zAgg2016'));
			
		    $persnSend = $nbr * $nbtexto;
		    $message = htmlspecialchars(trim($message));
		    
			 if ($nbsms>=$persnSend) {		 	
			 	
			 	$SENDER_ADDRESS = str_replace(" ", "_", $name_compte);

			    if($groupe!='undefined'){

			    	foreach ($Selectgroupe as $value) {
			    		$req =$db->query("SELECT contact_id FROM appartenir WHERE groupe_id=$value");
				    	$qs=$req->fetchAll(PDO::FETCH_OBJ);
				    	
				    	foreach ($qs as $q) {
				    		//echo "fa";
				    		// var_dump($q);

				    		$seleContact =$db->query("SELECT numero_contact FROM contact WHERE id=$q->contact_id AND etat_sup='1' ");
				    		$sele=$seleContact->fetch();

				    		//var_dump($sele['numero_contact']);
				    		// Creating request body
                            $requestBody = new SMSTextualRequest();
                            $requestBody->setFrom($SENDER_ADDRESS);
                            $requestBody->setTo([$sele['numero_contact']]);
                            $requestBody->setText(htmlspecialchars(trim($message)));

                            try {
                            //Envoi du sms
                            $response = $client->execute($requestBody);
                            }catch (Exception $exc) {
                            //insertion dans la table historique transaction
                            	$dataMessage = $db->prepare("INSERT INTO sms(destinataire_msg,message,user_id,status_msg,created_at,updated_at) VALUES (:destinataire_msg,:message,:user_id,:status_msg,NOW(),NOW())");
			            		$dataMessage->execute(array(
							        'destinataire_msg'=>$sele['numero_contact'],
							        'message'=>$message,
							        'user_id'=>$id_user,
							        'status_msg'=>0
					        	));
                            }


				    		$dataMessage = $db->prepare("INSERT INTO sms(destinataire_msg,message,user_id,status_msg,created_at,updated_at) VALUES (:destinataire_msg,:message,:user_id,:status_msg,NOW(),NOW())");
		            		$dataMessage->execute(array(
						        'destinataire_msg'=>$sele['numero_contact'],
						        'message'=>$message,
						        'user_id'=>$id_user,
						        'status_msg'=>1
				        	));

		             		$reduis = $db->query("SELECT nbresms_compte FROM users WHERE id=$id_user");
					        $red=$reduis->fetch();

					        $NewNbSms = $red['nbresms_compte'] - $nbtexto ;

					        $updateNbSms=$db->prepare("UPDATE users SET nbresms_compte = :NewNbSms WHERE id=:id_user");
					        $updateNbSms->execute(array(
					        	'NewNbSms'=>$NewNbSms,
					        	'id_user'=>$id_user
					        	));

				    	}
			    	}
			    }

			    if ($contact!='undefined') {
			    	
			    	foreach ($Selectcontact as $value) {
				    		
				    		$seleContact =$db->query("SELECT numero_contact FROM contact WHERE id=$value AND etat_sup='1' ");
				    		$sele=$seleContact->fetch();

				    		// Creating request body
                            $requestBody = new SMSTextualRequest();
                            $requestBody->setFrom($SENDER_ADDRESS);
                            $requestBody->setTo([$sele['numero_contact']]);
                            $requestBody->setText(htmlspecialchars(trim($message)));

                            try {
                            //Envoi du sms
                            $response = $client->execute($requestBody);
                            }catch (Exception $exc) {
                            //insertion dans la table historique transaction
                            	$dataMessage = $db->prepare("INSERT INTO sms(destinataire_msg,message,user_id,status_msg,created_at,updated_at) VALUES (:destinataire_msg,:message,:user_id,:status_msg,NOW(),NOW())");
			            		$dataMessage->execute(array(
							        'destinataire_msg'=>$sele['numero_contact'],
							        'message'=>$message,
							        'user_id'=>$id_user,
							        'status_msg'=>0
					        	));

                            }


				    		$dataMessage = $db->prepare("INSERT INTO sms(destinataire_msg,message,user_id,status_msg,created_at,updated_at) VALUES (:destinataire_msg,:message,:user_id,:status_msg,NOW(),NOW())");
		            		$dataMessage->execute(array(
						        'destinataire_msg'=>$sele['numero_contact'],
						        'message'=>$message,
						        'user_id'=>$id_user,
						        'status_msg'=>1
				        	));

		             		$reduis = $db->query("SELECT nbresms_compte FROM users WHERE id=$id_user");
					        $red=$reduis->fetch();

					        $NewNbSms = $red['nbresms_compte'] - $nbtexto ;

					        $updateNbSms=$db->prepare("UPDATE users SET nbresms_compte = :NewNbSms WHERE id=:id_user");
					        $updateNbSms->execute(array(
					        	'NewNbSms'=>$NewNbSms,
					        	'id_user'=>$id_user
					        	));

				    	}
			    }

				$data=1;
	        	echo json_encode($data);

			 }else{
			 	$data=0;
			 	echo json_encode($data);
			 }

		}


		 //Receperation des information du compte

		if($namescript=="compte" ){

			$id=$_REQUEST['id'];

			$req = $db->prepare("SELECT * FROM users WHERE id = :id");
			$req->execute(array(
				'id'=>$id
			));


			$selecte = $req->fetch();
			$data[]=array (
				"nom_compte"  => $selecte['nom_compte'],
				"nbresms_compte"  => $selecte['nbresms_compte']
			);
			echo json_encode($data);
		}

?>
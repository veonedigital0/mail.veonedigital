<!DOCTYPE html>
<html lang="fr">
<head>

    <title>RECHARGEMENT</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="../assets/css/style.css" rel="stylesheet" />
    <link href="../assets/css/main-style.css" rel="stylesheet" />
    <script src="../assets/plugins/jquery-1.10.2.js"></script>
    <script src="https://www.cinetpay.com/cdn/seamless_sdk/latest/cinetpay.prod.min.js"></script>
<!--    <script src="https://www.cinetpay.com/cdn/seamless_sdk/latest/cinetpay.prod.min.js"></script>-->
<!--    <script type="text/javascript" language="javascript" src="js/jquery-3.1.1.js"></script>-->

</head>

<!-- Styles -->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"/>-->


<body  id="body">
<nav class="navbar navbar-default" id="navbar1">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> <img src="../assets/img/logo_lettre.fw.png" alt="" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">


            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Plateforme sms</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" id="heading" align="center">RECHARGEMENT DE COMPTE</div><br>
                <div class="panel-body">
                    <div align="center">
                        <div class="alert-success" id="message">
                            <p>Le montant d'un rechargement doit etre sup&eacuterieur ou &eacute;gale &agrave; 30 FCFA</p>
                        </div>
                        <form id="rechargement" name="rechargement" class="form-horizontal" role="form" method="POST" action="pay.php">
                            <div class="form-group">
                                <label for="nom" class="col-md-4 control-label">Nom du compte</label>

                                <div class="col-md-6">
                                    <input type="text" id="nom"  class="form-control" name="nom" maxlength="11" onkeyup="this.value=this.value.toUpperCase()" autocomplete="off"
                                           placeholder="Nom du compte" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="montant_a_payer" class="col-md-4 control-label">Montant</label>

                                <div class="col-md-6">
                                    <input type="number" min="35" id="montant_a_payer"  class="form-control" name="montant_a_payer" autocomplete="off"  required="required">
                                </div>
                            </div>

                            <input type="hidden" id="trans_id" name="trans_id" value="<?php echo mt_rand()?>">

                            <input type="hidden" id="rechargement" name="rechargement" value="rechargement" >

                            <input type="hidden" id="date_transaction" name="date_transaction" value="<?php echo date("Y-m-d H:i:s") ?>">


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success" id="recharger" name="recharger" >
                                        <i class="fa fa-btn fa-sign-in" id="submit">Recharger</i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <img src="../image/ajax-loader.gif" id="load" style="display:none"/>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--
<script language="javascript">

    $(document).ready(function()
    {

        $("#valider").click(function()
        {
            style="display:none";
            $.ajax
            ({
                type: "POST"
                url: "include/pay.php"
                timeout: 10000
                data: {"etat="+$("#etat").val()
                +"&nom="+$("#nom").val()
                +"&adresse="+$("#adresse").val()
                +"&montant_a_payer="+$("#montant_a_payer").val()
                +"&trans_id="+$("#trans_id").val()
                +"&designation="+$("#designation").val()
                +"&date_transaction="+$("#date_transaction").val()

                }
                success: function(response) {
                    var rep = JSON.parse(response);
                    $('#load').hide();
                    beforeSend: function() {
                        $('#load').show();
                    }

                });


        });
    });

    })

</script>-->

</body>
</html>


